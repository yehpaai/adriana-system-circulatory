import group_config
from random import randint
from SCENE import scene_config
from ITEM import create_item
from effect_system import effect_anim, effect_bodies, effect_take
from config_script import WINDOWS_SIZE
from sfx_compilation import TAKE_ITEM_EXP, DEATH_BALL, TAKE_ITEM_AMMO, DEATH_PEOPLE, TAKE_ITEM_CRYSTAL, TELEPORT, \
    DAMAGE_SHUTLE, DIE_SHUTLE
import particle

timeDamage: int = 0

fileEffectOne = ("ASSETS/SPRITE/EFFECT/1/1.png", "ASSETS/SPRITE/EFFECT/1/2.png", "ASSETS/SPRITE/EFFECT/1/3.png",
                 "ASSETS"
                 "/SPRITE/EFFECT/1/4.png",
                 "ASSETS/SPRITE/EFFECT/1/5.png", "ASSETS/SPRITE/EFFECT/1/6.png")

fileEffectTwo = ("ASSETS/SPRITE/EFFECT/3/1.png", "ASSETS/SPRITE/EFFECT/3/2.png", "ASSETS/SPRITE/EFFECT/3/3.png",
                 "ASSETS"
                 "/SPRITE/EFFECT/3/4.png",
                 "ASSETS/SPRITE/EFFECT/3/5.png", "ASSETS/SPRITE/EFFECT/3/6.png")

fileEffectThree = ("ASSETS/SPRITE/EFFECT/4/1.png", "ASSETS/SPRITE/EFFECT/4/2.png", "ASSETS/SPRITE/EFFECT/4/3.png")

fileEffectFour = (
    "ASSETS/SPRITE/EFFECT/Boom/1.png", "ASSETS/SPRITE/EFFECT/Boom/2.png", "ASSETS/SPRITE/EFFECT/Boom/3.png",
    "ASSETS/SPRITE/EFFECT/Boom/4.png")

fileEffectFive = (
    "ASSETS/SPRITE/EFFECT/BoomNeon/1.png", "ASSETS/SPRITE/EFFECT/BoomNeon/2.png", "ASSETS/SPRITE/EFFECT/BoomNeon/3.png",
    "ASSETS/SPRITE/EFFECT/BoomNeon/4.png")

fileEffectSix = (
    "ASSETS/SPRITE/EFFECT/pickUp/1.png", "ASSETS/SPRITE/EFFECT/pickUp/2.png", "ASSETS/SPRITE/EFFECT/pickUp/3.png",
    "ASSETS/SPRITE/EFFECT/pickUp/4.png", "ASSETS/SPRITE/EFFECT/pickUp/5.png", "ASSETS/SPRITE/EFFECT/pickUp/6.png")

bodies = ("ASSETS/SPRITE/EFFECT/bodies/1.png", "ASSETS/SPRITE/EFFECT/bodies/2.png", "ASSETS/SPRITE/EFFECT/bodies/3.png")


def createEffectSimple(x, y, fileEffect, effect=6, size=55, type_effect=0):
    return effect_anim(x + 23, y + 25, fileEffect, effect, size, type_effect)


def createEffectBodies(x, y, filename, size=24, isMetal=False):
    return effect_bodies(x, y, filename, size, isMetal)


def createEffectTake(x, y, filename, size=(16, 16)):
    return effect_take(x, y, filename, size)


def createParticle(x, y, pos=0, filename="ASSETS/SPRITE/EFFECT/2/1.png", isAnim=True, size=26):
    particle.particle_simple(x + 24, y + 25, pos, filename, isAnim, size)


isKillBullet: bool = True


def collisionItem():
    for item in group_config.item_group:
        if scene_config.p.rect.colliderect(item.rect):
            match item.idItem:
                case 0:
                    TAKE_ITEM_EXP.play()
                    createEffectSimple(item.rect.x, item.rect.y, fileEffectSix, 6, 29)
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+10 EXP")
                    scene_config.p.EXP += 10
                    ScoreSet = 25 + scene_config.p.MultipleScore
                    scene_config.Score += 25 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    if scene_config.isParticle:
                        for i in range(2):
                            createParticle(item.rect.x, item.rect.y, i, "ASSETS/SPRITE/EFFECT/7.png", False, 23)
                    item.kill()
                case 1:
                    TAKE_ITEM_EXP.play()
                    createEffectSimple(item.rect.x, item.rect.y, fileEffectSix, 6, 29)
                    ScoreSet = 45 + scene_config.p.MultipleScore
                    scene_config.Score += 45 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+20 EXP")
                    scene_config.p.EXP += 20
                    if scene_config.isParticle:
                        for i in range(2):
                            createParticle(item.rect.x, item.rect.y, i, "ASSETS/SPRITE/EFFECT/7.png", False, 23)
                    item.kill()
                case 2:
                    TAKE_ITEM_AMMO.play()
                    createEffectSimple(item.rect.x, item.rect.y, fileEffectSix, 6, 31)
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+150 AMMO")
                    scene_config.p.Ammo += 145
                    ScoreSet = 15 + scene_config.p.MultipleScore
                    scene_config.Score += 15 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    if scene_config.isParticle:
                        for i in range(2):
                            createParticle(item.rect.x, item.rect.y, i, "ASSETS/SPRITE/EFFECT/7.png", False, 24)
                    item.kill()
                case 3:
                    TELEPORT.play()
                    createEffectSimple(item.rect.x, item.rect.y, fileEffectSix, 6, 31)
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+95 SCORE", (255, 255, 255))
                    ScoreSet = 35 + scene_config.p.MultipleScore
                    scene_config.Score += 35 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    scene_config.p.rect.x = randint(250, 945)
                    scene_config.p.rect.y = randint(150, 615)
                    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white_teleport.png")
                    if scene_config.isParticle:
                        for i in range(2):
                            createParticle(item.rect.x, item.rect.y, i, "ASSETS/SPRITE/EFFECT/7.png", False, 24)
                    item.kill()
                case 4:
                    createEffectSimple(item.rect.x, item.rect.y, fileEffectSix, 6, 31)
                    health = randint(15, 65)
                    scene_config.p.Health += health
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+" + str(health) + " HEALTH",
                                          (255, 255, 255))
                    ScoreSet = 25 + scene_config.p.MultipleScore
                    scene_config.Score += 25 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/green_health.png")
                    item.kill()
                case 6:
                    TAKE_ITEM_CRYSTAL.play()
                    create_item.createTXT(item.rect.x + 15, item.rect.y + 15, "+1 CRYSTAL", (63, 222, 255))
                    createEffectSimple(item.rect.x, item.rect.y, fileEffectSix, 6, 31)
                    scene_config.p.Health -= 2
                    for i in range(4):
                        createEffectTake(scene_config.p.rect.x + 17, scene_config.p.rect.y + 15,
                                         "ASSETS/SPRITE/UI/ICON/DamageHealth.png")
                    create_item.createTXT(WINDOWS_SIZE[0] // 2, 696, "-2", (255, 0, 0))
                    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/syan.png", 15)
                    scene_config.CRYSTAL_COUNT += 1
                    ScoreSet = 125 + scene_config.p.MultipleScore
                    scene_config.Score += 125 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    item.kill()


isTakeDamage: bool = False
timeTakeDamage: int = 0
PlayerAlpha: int = 250
AlphaTime: int = 0

timeBoom: int = 0


def collisionBullet():
    global timeDamage, timeTakeDamage, isTakeDamage, PlayerAlpha, AlphaTime, timeBoom
    match isTakeDamage:
        case True:
            for e_b in group_config.simple_bullet:
                e_b.kill()
            for e_b in group_config.pistol_bullet:
                e_b.kill()
            for e_b in group_config.anyBullet:
                e_b.kill()
            match timeTakeDamage:
                case 10:
                    PlayerAlpha = 300
                    scene_config.p.image.set_alpha(PlayerAlpha)
                    isTakeDamage = False
                    timeTakeDamage = 0
                    scene_config.p.isControl = True
                case _:
                    match AlphaTime:
                        case 50:
                            AlphaTime = 0
                            PlayerAlpha = 250
                        case _:
                            AlphaTime += 1
                            PlayerAlpha -= 35
                    scene_config.p.image.set_alpha(PlayerAlpha)
                    if scene_config.p.rect.y <= 645:
                        scene_config.p.rect.y += 8
                    scene_config.p.isControl = False
                    timeTakeDamage += 1

    for player_b in group_config.player_bullet:
        for ballRosa in group_config.ball_rosa_group:
            if ballRosa.rect.collidepoint(player_b.rect.center):
                DEATH_BALL.play()
                luck: int = randint(0, 3)
                match luck:
                    case 0:
                        if scene_config.p.EXP < 500:
                            create_item.create_exp(ballRosa.rect.x, ballRosa.rect.y, 0)
                    case 3:
                        if scene_config.p.Health <= 150:
                            create_item.createX_CUSE(ballRosa.rect.x + 15, ballRosa.rect.y + 10)
                ScoreSet = 15 + scene_config.p.MultipleScore
                scene_config.Score += 15 + scene_config.p.MultipleScore
                create_item.createTXT(90
                                      , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                create_item.createTXT(ballRosa.rect.x + 15, ballRosa.rect.y + 15, "+" + str(ScoreSet), (255, 255, 0))
                createEffectSimple(ballRosa.rect.x, ballRosa.rect.y, fileEffectOne, 6, 55, 1)
                if scene_config.isParticle:
                    for i in range(4):
                        createParticle(ballRosa.rect.x, ballRosa.rect.y, i)
                if isKillBullet:
                    player_b.kill()
                ballRosa.timeMove = 0
                ballRosa.isMove = False
                ballRosa.rect.y = -200
                ballRosa.rect.x = -200
                scene_config.KILL_COUNT += 1
        for ballSoul in group_config.ball_soul:
            if ballSoul.rect.collidepoint(player_b.rect.center):
                DEATH_BALL.play()
                create_item.createTXT(ballSoul.rect.x + 20, ballSoul.rect.y + 10,
                                      "-" + str(scene_config.p.damageBullet), (255, 0, 0))
                if ballSoul.Health <= 0:
                    player_b.kill()
                    if scene_config.p.EXP < 500:
                        create_item.create_exp(ballSoul.rect.x + 20, ballSoul.rect.y + 10, 0)
                    createEffectSimple(ballSoul.rect.x, ballSoul.rect.y, fileEffectOne, 6, 55, 1)
                    if scene_config.isParticle:
                        for i in range(4):
                            createParticle(ballSoul.rect.x, ballSoul.rect.y, i)
                    ScoreSet = 20 + scene_config.p.MultipleScore
                    scene_config.Score += 20 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    create_item.createTXT(ballSoul.rect.x + 15, ballSoul.rect.y + 15, "+" + str(ScoreSet),
                                          (255, 255, 0))
                    ballSoul.timeMove = 0
                    ballSoul.isMove = False
                    ballSoul.Health = 145
                    ballSoul.rect.y = -300
                    ballSoul.rect.x = -300
                    scene_config.KILL_COUNT += 1
                else:
                    ballSoul.image.set_alpha(10)
                    ballSoul.Health -= scene_config.p.damageBullet
                    if isKillBullet:
                        player_b.kill()

        for Shutle in group_config.shutle_group:
            if Shutle.rect.collidepoint(player_b.rect.center):
                create_item.createTXT(Shutle.rect.x + 25, Shutle.rect.y + 5, "-" + str(scene_config.p.damageBullet),
                                      (255, 0, 0))
                if Shutle.idS == 2 or 1:
                    Shutle.isTakeDamage = True
                if Shutle.Health <= 0:
                    DIE_SHUTLE.play()
                    player_b.kill()
                    if scene_config.p.EXP < 500:
                        create_item.create_exp(Shutle.rect.x, Shutle.rect.y, 1)
                    for e in group_config.effect_group:
                        e.kill()
                    createEffectSimple(Shutle.rect.x + 20, Shutle.rect.y + 10, fileEffectFive, 4, 79, 1)
                    if scene_config.isParticle:
                        for i in range(4):
                            createParticle(Shutle.rect.x, Shutle.rect.y, i)
                    scene_config.KILL_COUNT += 2
                    ScoreSet = 35 + scene_config.p.MultipleScore
                    scene_config.Score += 35 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                    if scene_config.isParticleMore:
                        for i in range(scene_config.TRASH_COUNT):
                            size = randint(8, 19)
                            x = randint(1, 55)
                            filename = ("ASSETS/SPRITE/EFFECT/6.png", "ASSETS/SPRITE/EFFECT/10.png",
                                        "ASSETS/SPRITE/EFFECT/bodies/1.png", "ASSETS/SPRITE/EFFECT/11.png")
                            createEffectBodies(Shutle.rect.x + x, Shutle.rect.y + x, filename[randint(0, 3)], size,
                                               True)
                    Shutle.timeMove = 0
                    Shutle.isMove = False
                    Shutle.rect.y = -300
                    Shutle.rect.x = -300
                    match Shutle.idS:
                        case 0:
                            Shutle.Health = 695
                        case 1:
                            Shutle.Frame = 0
                            Shutle.Health = 120
                        case 2:
                            Shutle.Health = 815
                else:
                    match timeBoom:
                        case 11:
                            timeBoom = 0
                            createEffectSimple(Shutle.rect.x + 5, Shutle.rect.y + 25, fileEffectFour, 4, 44)
                            DAMAGE_SHUTLE.play()
                        case _:
                            timeBoom += 1
                    Shutle.Health -= scene_config.p.damageBullet
                    Shutle.image.set_alpha(0)
                    if isKillBullet:
                        player_b.kill()

        for shota in group_config.shota_group:
            if shota.rect.collidepoint(player_b.rect.center):
                DEATH_PEOPLE.play()
                create_item.createTXT(shota.rect.x + 30, shota.rect.y + 10, "-" + str(scene_config.p.damageBullet),
                                      (255, 0, 0))
                shota.isTakeDamage = True
                if shota.Health <= 0:
                    player_b.kill()
                    luckS = randint(0, 1)
                    match luckS:
                        case 0:
                            if scene_config.p.EXP < 500:
                                create_item.create_exp(shota.rect.x, shota.rect.y, 0)
                        case 1:
                            create_item.create_ammo(shota.rect.x, shota.rect.y)
                    create_item.create_teleporter(shota.rect.x + 10, shota.rect.y + 5)
                    createEffectSimple(shota.rect.x, shota.rect.y, fileEffectOne, 6, 55, 1)
                    selectBodies = randint(0, 2)
                    match selectBodies:
                        case 0:
                            createEffectBodies(shota.rect.x + 16, shota.rect.y + 16, bodies[0])
                        case 1:
                            createEffectBodies(shota.rect.x + 16, shota.rect.y + 16, bodies[1])
                        case 2:
                            createEffectBodies(shota.rect.x + 16, shota.rect.y + 16, bodies[2])
                    createEffectBodies(shota.rect.x + 19, shota.rect.y + 17, "ASSETS/SPRITE/EFFECT/bodies/4.png")
                    if scene_config.isParticle:
                        for i in range(4):
                            createParticle(shota.rect.x, shota.rect.y, i)
                    scene_config.KILL_COUNT += 1
                    shota.timeMove = 0
                    shota.isMove = False
                    shota.rect.y = -200
                    if shota.type == 1:
                        shota.Health = 250
                    else:
                        shota.Health = 125
                    shota.rect.x = -200
                    shota.Health = 145
                    ScoreSet = 45 + scene_config.p.MultipleScore
                    scene_config.Score += 45 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                else:
                    shota.Health -= scene_config.p.damageBullet
                    createEffectSimple(shota.rect.x + 19, shota.rect.y + 42, fileEffectThree, 3, 43)
                    if isKillBullet:
                        player_b.kill()
        for type_one in group_config.magical_girl_group:
            if type_one.rect.collidepoint(player_b.rect.center):
                DEATH_PEOPLE.play()
                create_item.createTXT(type_one.rect.x + 30, type_one.rect.y + 5 - 15,
                                      "-" + str(scene_config.p.damageBullet),
                                      (255, 0, 0))
                type_one.isTakeDamage = True
                if type_one.Health <= 0:
                    player_b.kill()
                    if scene_config.p.EXP < 500:
                        create_item.create_exp(type_one.rect.x, type_one.rect.y, 1)
                    createEffectSimple(type_one.rect.x, type_one.rect.y, fileEffectOne, 6, 55, 1)
                    selectBodies = randint(0, 2)
                    match selectBodies:
                        case 0:
                            createEffectBodies(type_one.rect.x + 16, type_one.rect.y + 16, bodies[0])
                        case 1:
                            createEffectBodies(type_one.rect.x + 16, type_one.rect.y + 16, bodies[1])
                        case 2:
                            createEffectBodies(type_one.rect.x + 16, type_one.rect.y + 16, bodies[2])
                    match scene_config.isParticle:
                        case True:
                            for i in range(4):
                                createParticle(type_one.rect.x, type_one.rect.y, i)
                    scene_config.KILL_COUNT += 1
                    type_one.timeMove = 0
                    type_one.isMove = False
                    type_one.rect.y = -200
                    type_one.rect.x = -200
                    type_one.Health = 155
                    ScoreSet = 55 + scene_config.p.MultipleScore
                    scene_config.Score += 55 + scene_config.p.MultipleScore
                    create_item.createTXT(90
                                          , WINDOWS_SIZE[0] // 2 - 590, "+" + str(ScoreSet), (255, 255, 0), 1)
                else:
                    type_one.image.set_alpha(15)
                    type_one.Health -= scene_config.p.damageBullet
                    createEffectSimple(type_one.rect.x + 19, type_one.rect.y + 42, fileEffectThree, 3, 43)
                    if isKillBullet:
                        player_b.kill()

    for e_b in group_config.simple_bullet:
        if timeDamage >= 325:
            if e_b.rect.collidepoint(scene_config.p.rect.center):
                if scene_config.p.EXP > 0:
                    scene_config.p.EXP -= 4
                timeDamage = 0
                damage = None
                match scene_config.DIFFICULTY:
                    case 0:
                        damage = 15
                        scene_config.p.Health -= damage
                    case 1:
                        damage = 25
                        scene_config.p.Health -= damage
                    case 2:
                        damage = 35
                        scene_config.p.Health -= damage
                isTakeDamage = True
                for i in range(4):
                    createEffectTake(scene_config.p.rect.x + 17, scene_config.p.rect.y + 15,
                                     "ASSETS/SPRITE/UI/ICON/DamageHealth.png")
                scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/red.png", 11)
                create_item.createTXT(WINDOWS_SIZE[0] // 2 - 1, 695, "-"+str(damage)+" HEALTH", (255, 0, 0))
                e_b.kill()
        else:
            timeDamage += 1
    for e_b in group_config.pistol_bullet:
        if timeDamage >= 325:
            if e_b.rect.collidepoint(scene_config.p.rect.center):
                timeDamage = 0
                if scene_config.p.EXP > 0:
                    scene_config.p.EXP -= 5

                damage = None

                match scene_config.DIFFICULTY:
                    case 0:
                        damage = randint(25, 35)
                        scene_config.p.Health -= damage
                    case 1:
                        damage = randint(35, 45)
                        scene_config.p.Health -= damage
                    case 2:
                        damage = randint(45, 55)
                        scene_config.p.Health -= damage
                isTakeDamage = True
                for i in range(4):
                    createEffectTake(scene_config.p.rect.x + 17, scene_config.p.rect.y + 15,
                                     "ASSETS/SPRITE/UI/ICON/DamageHealth.png")
                scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/red.png", 12)
                create_item.createTXT(WINDOWS_SIZE[0] // 2 - 1, 695, "-" + str(damage) + " HEALTH", (255, 0, 0))
                e_b.kill()
        else:
            timeDamage += 1

    for e_b in group_config.anyBullet:
        if timeDamage >= 325:
            if e_b.rect.collidepoint(scene_config.p.rect.center):
                timeDamage = 0
                if scene_config.p.EXP > 0:
                    scene_config.p.EXP -= 12
                damage = None
                match scene_config.DIFFICULTY:
                    case 0:
                        damage = 45
                    case 1:
                        damage = 55
                    case 2:
                        damage = 90
                scene_config.p.Health -= damage
                isTakeDamage = True
                for i in range(4):
                    createEffectTake(scene_config.p.rect.x + 17, scene_config.p.rect.y + 15,
                                     "ASSETS/SPRITE/UI/ICON/DamageHealth.png")
                scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/red.png", 9)
                create_item.createTXT(WINDOWS_SIZE[0] // 2 - 1, 695, "-"+str(damage)+" HEALTH", (255, 0, 0))
                e_b.kill()
        else:
            timeDamage += 1
