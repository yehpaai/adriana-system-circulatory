from time import time

from pygame import image
from config_script import sc, LANGUAGE_SET
from sfx_compilation import TAKE_SCREENSHOT
from NOTFICATION import notfication_manager

TEXT_SET = ""

match LANGUAGE_SET:
    case "UA":
        TEXT_SET = "Знімок Зроблено"
    case "EN":
        TEXT_SET = "TAKE PHOTO"


def take():
    TAKE_SCREENSHOT.play()
    image.save(sc, str(int(time())) + ".png")
    notfication_manager.notfication_create("ASSETS/SPRITE/UI/Notication/2.png", TEXT_SET)
