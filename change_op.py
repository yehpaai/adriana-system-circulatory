import os
from config_script import *


def change():
    match LANGUAGE_SET:
        case "UA":
            path = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                ".language_data.langa")
            os.remove(path)
            file = open(".language_data.langa", "w")
            file.write("EN")
            file.close()
        case "EN":
            path = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                ".language_data.langa")
            os.remove(path)
            file = open(".language_data.langa", "w")
            file.write("UA")
            file.close()
