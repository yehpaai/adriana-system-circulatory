import pygame

from group_config import effect_group
from config_script import createImage


class particle_simple(pygame.sprite.Sprite):
    def __init__(self, x, y, pos, filename="ASSETS/SPRITE/EFFECT/2/1.png", isAnim=True, size=20):
        pygame.sprite.Sprite.__init__(self)
        self.filename = filename
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, (size, size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(effect_group)

        self.Frame: int = 0

        self.TimeKill: int = 0

        self.Alpha: int = 250

        self.pos: int = pos

        self.isAnim: bool = isAnim

        self.size: int = size

    def update(self):
        if self.isAnim:
            match self.Frame:
                case 10:
                    self.image = createImage("ASSETS/SPRITE/EFFECT/2/1.png")
                case 20:
                    self.image = createImage("ASSETS/SPRITE/EFFECT/2/2.png")
                    self.Frame = 0
            self.Frame += 1
            self.Alpha -= 7
            self.image.set_alpha(self.Alpha)
            self.image = pygame.transform.scale(self.image, (self.size, self.size))
        else:
            self.Alpha -= 10
            self.image.set_alpha(self.Alpha)

        match self.TimeKill:
            case 95:
                self.kill()
            case _:
                self.TimeKill += 1

        match self.pos:
            case 0:
                self.rect.y -= 10
            case 1:
                self.rect.y += 10
            case 2:
                self.rect.x -= 10
            case 3:
                self.rect.x += 10
