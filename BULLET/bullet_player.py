from random import randint

import pygame
from pygame import BLEND_RGB_ADD

from glow import createCircle
from group_config import player_bullet
from config_script import createImage, sc


class bullet_standart(pygame.sprite.Sprite):
    def __init__(self, x, y, speed, rot, pos):
        pygame.sprite.Sprite.__init__(self)
        self.Size: int = 18
        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/1/1.png")
        self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
        self.pos: int = pos
        match self.pos:
            case 1:
                self.rect = self.image.get_rect(center=(x - 1, y + 19))
            case _:
                self.rect = self.image.get_rect(center=(x, y))
        self.add(player_bullet)
        self.speed: int = speed
        self.rot: int = rot
        self.Frame: int = 0

        self.radius = self.Size - 8

    def update(self):
        sc.blit(createCircle(self.radius, (34, 39, 37)),
                (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                special_flags=BLEND_RGB_ADD)
        match self.pos:
            case 0:
                if self.rot == 0:
                    if self.rect.y >= 0:
                        self.rect.y -= self.speed
                    else:
                        self.kill()
                if self.rot == 1:
                    if self.rect.y >= 0 and self.rect.x <= 1280:
                        self.rect.y -= self.speed
                        self.rect.x += self.speed - 3
                    else:
                        self.kill()
                if self.rot == 2:
                    if self.rect.y >= 0 and self.rect.x >= 0:
                        self.rect.y -= self.speed
                        self.rect.x -= self.speed - 3
                    else:
                        self.kill()
            case 1:
                if self.rect.y < 655:
                    self.rect.y += self.speed
                else:
                    self.kill()
        match self.Frame:
            case 12:
                self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/1/1.png")
                self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
                self.Frame = 0
            case 6:
                self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/1/2.png")
                self.image = pygame.transform.scale(self.image, (self.Size, self.Size))
        self.Frame += 1


class bulletPoison(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/11.png")
        self.image = pygame.transform.scale(self.image, (14, 16))
        self.rect = self.image.get_rect(center=(x, y))

        self.size: int = 14
        self.radius: int = 0

        self.add(player_bullet)

    def update(self):
        if self.radius < self.size * 1 + 5:
            self.radius += 4
        else:
            self.radius = 1
        sc.blit(createCircle(self.radius, (0, 255, 56)),
                (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                special_flags=BLEND_RGB_ADD)
        if self.rect.y > 5:
            self.rect.y -= 10
        else:
            self.kill()


class bullet_plasma(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/3.png")
        self.image = pygame.transform.scale(self.image, (14, 13))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(player_bullet)
        self.speedMove: int = 0

        self.radius = 0

    def update(self):
        if self.radius < 7 * 2:
            self.radius += 2
        else:
            self.radius = 1
        sc.blit(createCircle(self.radius, (35, 40, 55)),
                (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                special_flags=BLEND_RGB_ADD)
        if self.rect.y >= 5:
            if self.speedMove < 25:
                self.speedMove += 1
            self.rect.y -= self.speedMove
        else:
            self.kill()


class bullet_pos_auto(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.add(player_bullet)
        self.pos: int = randint(0, 1)

        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/6.png")
        self.image = pygame.transform.scale(self.image, (15, 15))
        self.rect = self.image.get_rect(center=(x, y))
        match self.pos:
            case 0:
                self.image = pygame.transform.flip(self.image, True, False)

    def update(self):
        match self.pos:
            case 0:
                if self.rect.y >= 5 and self.rect.x >= 0:
                    self.rect.y -= 15
                    self.rect.x -= 11
                else:
                    self.kill()
            case 1:
                if self.rect.y >= 5 and self.rect.x <= 1220:
                    self.rect.y -= 15
                    self.rect.x += 11
                else:
                    self.kill()


class bullet_pistol(pygame.sprite.Sprite):
    def __init__(self, x, y, pos=0, filename="ASSETS/SPRITE/BULLET/PLAYER/2/up.png", speed=20):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, (9, 9))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(player_bullet)
        self.pos: int = pos
        self.speed: int = speed

    def update(self):
        if self.rect.y > 10:
            match self.pos:
                case 0:
                    self.rect.y -= self.speed
                case 1:
                    self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/2/left.png")
                    self.image = pygame.transform.scale(self.image, (11, 11))
                    self.rect.x -= self.speed + 1
                    self.rect.y -= self.speed
                case 2:
                    self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/2/right.png")
                    self.image = pygame.transform.scale(self.image, (11, 11))
                    self.rect.x += self.speed + 1
                    self.rect.y -= self.speed
        else:
            self.kill()


class bulletShotgun(pygame.sprite.Sprite):
    def __init__(self, x, y, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/10.png")
        self.image = pygame.transform.scale(self.image, (7, 8))
        self.rect = self.image.get_rect(center=(x, y))

        self.add(player_bullet)

        self.pos: int = pos

        self.xSpeed: int = 1

    def update(self):
        if self.rect.y > 10:
            match self.pos:
                case 1:
                    self.rect.y -= 17
                    self.rect.x += self.xSpeed
                    if self.xSpeed <= 5: self.xSpeed += 1
                case 2:
                    self.rect.y -= 17
                    self.rect.x -= self.xSpeed
                    if self.xSpeed <= 5: self.xSpeed += 1
                case _:
                    self.rect.y -= 18
        else:
            self.kill()
