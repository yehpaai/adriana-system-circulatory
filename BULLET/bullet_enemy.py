from random import randint

import pygame
from pygame import BLEND_RGB_ADD

from glow import createCircle
from group_config import simple_bullet, pistol_bullet, anyBullet
from SCENE import scene_config
from config_script import createImage, sc


class bulletHell(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, posFly, size=10):
        super().__init__()
        self.image = pygame.image.load(filename).convert()
        self.image = pygame.transform.scale(self.image, (size, size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(pistol_bullet)

        self.size: int = size
        self.radius: int = 1

        self.pos: int = posFly

        self.xForce: int = 0

    def update(self):
        if self.radius < self.size * 2 - 10:
            self.radius += 7
        else:
            self.radius = 1
        sc.blit(createCircle(self.radius, (75, 35, 35)),
                (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                special_flags=BLEND_RGB_ADD)
        match self.pos:
            case 1:
                if self.rect.y < 695:
                    self.rect.y += 8
                else:
                    self.kill()
            case 2:
                if self.rect.y < 695:
                    self.rect.y += 7
                    if self.xForce <= 6:
                        self.xForce += 1
                    self.rect.x -= self.xForce
                else:
                    self.kill()
            case 3:
                if self.rect.x > 0:
                    self.rect.x -= 8
                else:
                    self.kill()
            case 4:
                if self.rect.y > 0:
                    self.rect.y -= 7
                    if self.xForce <= 6:
                        self.xForce += 1
                    self.rect.x -= self.xForce
                else:
                    self.kill()
            case 5:
                if self.rect.y > 0:
                    self.rect.y -= 8
                else:
                    self.kill()
            case 6:
                if self.rect.y > 0:
                    self.rect.y -= 7
                    if self.xForce <= 6:
                        self.xForce += 1
                    self.rect.x += self.xForce
                else:
                    self.kill()
            case 7:
                if self.rect.x < 1200:
                    self.rect.x += 8
                else:
                    self.kill()
            case 8:
                if self.rect.y < 695:
                    self.rect.y += 7
                    if self.xForce <= 6:
                        self.xForce += 1
                    self.rect.x += self.xForce
                else:
                    self.kill()
            case _:
                self.kill()


class simpleBullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, isAnim=False, speed=3):
        super().__init__()
        match isAnim:
            case True:
                self.filename: [] = filename
                self.image = createImage(self.filename[0])
                self.image = pygame.transform.scale(self.image, (16, 16))
            case False:
                self.filename = filename
                self.image = createImage(self.filename)
                self.image = pygame.transform.scale(self.image, (9, 9))
                self.image = pygame.transform.flip(self.image, False, True)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(simple_bullet)

        self.radius = 16 * 1

        self.speed: int = speed

        self.Frame: int = 0

        self.isAnim: bool = isAnim

        self.posRun: int = 0

        self.autoAim: int = randint(0, 1)

        match self.autoAim:
            case 1:
                if self.rect.centerx < scene_config.p.rect.centerx and self.rect.centery < scene_config.p.rect.centery:
                    self.posRun = 1
                if self.rect.centerx > scene_config.p.rect.centerx and self.rect.centery < scene_config.p.rect.centery:
                    self.posRun = 2

        self.timeAutoAim = 0

    def update(self):
        if self.radius < 17 * 2:
            self.radius += 6
        else:
            self.radius = 1
        sc.blit(createCircle(self.radius, (35, 0, 0)),
                (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                special_flags=BLEND_RGB_ADD)
        if self.isAnim:
            if self.rect.y <= 750:
                self.rect.y += self.speed
            else:
                self.kill()
            match self.posRun:
                case 1:
                    self.rect.x += self.speed
                case 2:
                    self.rect.x -= self.speed

            match self.Frame:
                case 11:
                    self.Frame = 0
                    self.image = createImage("ASSETS/SPRITE/BULLET/ENEMY/simple/1.png")
                    self.image = pygame.transform.scale(self.image, (16, 16))
                case 5:
                    self.image = createImage("ASSETS/SPRITE/BULLET/ENEMY/simple/2.png")
                    self.image = pygame.transform.scale(self.image, (16, 16))
            self.Frame += 1
        else:
            if self.rect.y <= 790:
                self.rect.y += self.speed + 2
            else:
                self.kill()


class Best_Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, pos, size=(15, 29)):
        super().__init__()
        self.image = pygame.image.load(filename).convert()
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(anyBullet)

        self.size = size[1] - 5

        self.radius = self.size * 2

        self.pos = pos

        match self.pos:
            case 2:
                self.image = pygame.transform.rotate(self.image, 90)
            case 3:
                self.image = pygame.transform.rotate(self.image, -90)

        self.speed = 6

    def update(self):
        if self.radius < self.size * 2 + 5:
            self.radius += 7
        else:
            self.radius = 4
        sc.blit(createCircle(self.radius, (45, 0, 0)),
                (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                special_flags=BLEND_RGB_ADD)
        match self.pos:
            case 0:
                if self.rect.y <= 680:
                    self.rect.y += self.speed
                else:
                    self.kill()
            case 1:
                if self.rect.y >= 5:
                    self.rect.y -= self.speed
                else:
                    self.kill()
            case 2:
                if self.rect.x >= 5:
                    self.rect.x += self.speed
                else:
                    self.kill()
            case 3:
                if self.rect.x <= 1270:
                    self.rect.x -= self.speed
                else:
                    self.kill()


class pistolBullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, speed=5, size=11, flip=True, pos=0):
        pygame.sprite.Sprite.__init__(self)
        self.filename = filename
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, (size, size))
        self.image = pygame.transform.flip(self.image, False, flip)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(pistol_bullet)

        self.size = size

        self.radius = self.size * 2 + 25

        self.speed: int = speed

        self.pos = pos

    def update(self):
        if self.radius < self.size * 2 + 25:
            self.radius += 5
        else:
            self.radius = 3
        sc.blit(createCircle(self.radius, (25, 0, 0)),
                (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                special_flags=BLEND_RGB_ADD)
        match self.pos:
            case 0:
                if self.rect.y <= 760:
                    self.rect.y += self.speed + 1
                else:
                    self.kill()
            case 1:
                if self.rect.y > 5:
                    self.rect.y -= self.speed + 1
                else:
                    self.kill()
