import pygame
from config_script import WINDOWS_SIZE, createImage
from group_config import notfication_group
from font_config import skillFont
from SCENE.scene_config import AA_TEXT


class notfication_draw(pygame.sprite.Sprite):
    def __init__(self, filename, text):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, (393, 365))
        self.rect = self.image.get_rect(center=(WINDOWS_SIZE[0] // 2, -85))

        self.text = skillFont.render(text, AA_TEXT, (255, 255, 255))
        self.rectTxt = self.text.get_rect(center=(self.rect.x + 250, self.rect.y+190))

        self.add(notfication_group)

        self.timeLife: int = 0
        self.FrameLeave: int = 0

    def update(self):
        if self.timeLife <= 205:
            self.timeLife += 1
            if self.rect.y < -130:
                self.rectTxt.y += 4
                self.rect.y += 4
        else:
            if self.FrameLeave <= 85:
                self.FrameLeave += 1
                self.rect.y -= 3
                self.rectTxt.y -= 3
            else:
                self.kill()
