import pygame
from group_config import item_group
from config_script import createImage


class x_cuse(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ITEM/x-cuse.png")
        self.image = pygame.transform.scale(self.image, (33, 33))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(item_group)

        self.idItem: int = 4

        self.ForceDown = 0

    def update(self):
        if self.rect.y < 670:
            if self.ForceDown < 3:
                self.ForceDown += 0.5
            self.rect.y += self.ForceDown
        else:
            self.kill()
