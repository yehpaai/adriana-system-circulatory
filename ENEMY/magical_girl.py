from random import randint

from group_config import magical_girl_group
from BULLET import bullet_enemy
from config_script import createImage

import pygame


class type_one(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/ENEMY/MagicalGirl/1/1.png")
        self.image = pygame.transform.scale(self.image, (91, 91))
        self.posRun: int = randint(0, 1)
        self.rect = self.image.get_rect(center=(-50, -50))
        self.add(magical_girl_group)

        self.Health: int = 145
        self.isTakeDamage: bool = False
        self.isTakeDamageFrame: float = 0

        self.upTime = randint(50, 95)
        self.upWork: int = 0
        self.timeWait: int = 0

        self.pos: int = 0

        self.timeShot: int = 0

        self.timeStartShot: int = 0

        self.timeLive: int = 0

        self.isMove: bool = False

        self.timeMove: int = 0
        self.timeMoveMax = randint(475, 3200)
        for i in magical_girl_group:
            if i.timeMoveMax == self.timeMoveMax:
                self.timeMoveMax = randint(475, 3200)

    def bullet(self):
        match self.timeStartShot:
            case 95:
                match self.timeShot:
                    case 105:
                        self.timeStartShot = 0
                        self.timeShot = 0
                        return bullet_enemy.pistolBullet(self.rect.x + 45, self.rect.y,
                                                         "ASSETS/SPRITE/BULLET/ENEMY/6.png", 7, 64, False)
                self.timeShot += 1
            case _:
                self.timeStartShot += 1

    def update(self):
        if self.timeMove == self.timeMoveMax*6:
            if not self.isMove:
                match self.posRun:
                    case 0:
                        self.rect = self.image.get_rect(center=(-10, randint(320, 560)))
                    case 1:
                        self.rect = self.image.get_rect(center=(1290, randint(320, 560)))
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.timeLive <= 1150:
                self.timeLive += 1
                if self.upWork < self.upTime:
                    match self.posRun:
                        case 0:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (91, 91))
                            self.rect.y -= 3
                            self.rect.x += 2
                            self.upWork += 1
                        case 1:
                            self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                            self.image = pygame.transform.scale(self.image, (91, 91))
                            self.image = pygame.transform.flip(self.image, True, False)
                            self.rect.y -= 3
                            self.rect.x -= 2
                            self.upWork += 1
                else:
                    match self.timeWait:
                        case 115:
                            self.timeWait = 0
                            self.pos = randint(0, 5)
                        case _:
                            self.bullet()
                            self.timeWait += 1
                    self.move()
            else:
                if self.rect.y > -30:
                    self.rect.y -= 4
                else:
                    self.timeMove = 0
                    self.isMove = False
                    self.rect.y = -400
                    self.rect.x = -400
            if self.isTakeDamage:
                if self.isTakeDamageFrame < 1.5:
                    self.isTakeDamageFrame += 0.5
                    self.image = createImage("ASSETS/SPRITE/ENEMY/MagicalGirl/1/damage.png")
                    self.image = pygame.transform.scale(self.image, (91, 91))
                else:
                    self.image = createImage("ASSETS/SPRITE/ENEMY/MagicalGirl/1/1.png")
                    self.image = pygame.transform.scale(self.image, (91, 91))
                    self.isTakeDamageFrame = 0
                    self.isTakeDamage = False

    def move(self):
        match self.pos:
            case 0:
                if self.rect.x >= 15 and self.rect.y >= 20:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (91, 91))
                    self.image = pygame.transform.flip(self.image, True, False)
                    self.rect.y -= 3
                    self.rect.x -= 2
                else:
                    self.pos = 1
            case 1:
                if self.rect.x <= 1200 and self.rect.y <= 600:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (91, 91))
                    self.rect.y += 3
                    self.rect.x += 2
                else:
                    self.pos = 4
            case 2:
                self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/1.png").convert_alpha()
                self.image = pygame.transform.scale(self.image, (91, 91))
            case 3:
                if self.rect.x >= 35:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (91, 91))
                    self.image = pygame.transform.flip(self.image, True, False)
                    self.rect.x -= 2
                else:
                    self.pos = 0
            case 4:
                if self.rect.x <= 1200:
                    self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/2.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (91, 91))
                    self.rect.x += 2
                else:
                    self.pos = 0
            case _:
                self.image = pygame.image.load("ASSETS/SPRITE/ENEMY/MagicalGirl/1/1.png").convert_alpha()
                self.image = pygame.transform.scale(self.image, (91, 91))
