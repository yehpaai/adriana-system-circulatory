from random import randint

import pygame

from config_script import createImage
from group_config import shota_group
from BULLET import bullet_enemy
from sfx_compilation import SHOOT_ENEMY_FIRST


class eva(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ENEMY/EVA/1.png")
        self.scale = randint(48, 58)
        self.image = pygame.transform.scale(self.image, (self.scale, self.scale))
        self.rect = self.image.get_rect(center=(-500, -500))

        self.type: int = 1

        self.Health: int = 250

        self.add(shota_group)

        self.timeUp: int = 0
        self.timeUpMax: int = randint(95, 200)

        self.timeShoot: int = 0
        self.preTimeShoot: int = 0
        self.shootTimeOther: int = 0
        self.isShoot: bool = False

        self.timeFrame: int = 0
        self.isFrameDown: bool = False

        self.speedDown: int = 0
        self.speedUp: int = 0

        self.switchPos: int = 0
        self.isSwitchPos: bool = False
        self.posMove: int = 0

        self.timeLive: int = 0

        self.isMove: bool = False

        self.timeMove: int = 0
        self.timeMoveMax = self.timeMoveMax = randint(520, 3790)
        for i in shota_group:
            if i.timeMoveMax == self.timeMoveMax:
                self.timeMoveMax = randint(520, 3790)

    def createBullet(self):
        if self.preTimeShoot >= 200:
            if self.timeShoot < 45 and self.isShoot:
                self.timeShoot += 1
                if self.timeShoot > 45:
                    self.isShoot = False
                match self.shootTimeOther:
                    case 10:
                        SHOOT_ENEMY_FIRST.play()
                        for i in range(9):
                            bullet_enemy.bulletHell(self.rect.x + 28, self.rect.y + 25, "ASSETS/SPRITE/BULLET/ENEMY/10.png",
                                                    i)
                        self.shootTimeOther = 0
                    case _:
                        self.shootTimeOther += 1
            else:
                if self.timeShoot > 0:
                    self.timeShoot -= 1
                else:
                    self.isShoot = True
        else:
            self.preTimeShoot += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                self.rect.x = randint(135, 935)
                self.rect.y = 750
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            if self.timeLive <= 1450:
                self.createBullet()
                if self.timeUp < self.timeUpMax:
                    self.timeUp += 1
                    self.rect.y -= 3
                else:
                    if self.switchPos <= 125 and not self.isSwitchPos:
                        if self.switchPos >= 125:
                            self.posMove = randint(0, 1)
                            self.isSwitchPos = True
                        self.switchPos += 1
                        if self.timeFrame < 25 and self.isFrameDown:
                            self.timeFrame += 1
                            if self.timeFrame >= 25:
                                self.isFrameDown = False
                            if self.speedDown < 1:
                                self.speedDown += 0.1
                            self.rect.y += self.speedDown
                        else:
                            self.speedDown = 0
                            if self.timeFrame > 0:
                                self.timeFrame -= 1
                                if self.speedUp < 1:
                                    self.speedUp += 0.1
                                self.rect.y -= self.speedUp
                            else:
                                self.speedUp = 0
                                self.isFrameDown = True
                    else:
                        if self.switchPos > 0:
                            match self.posMove:
                                case 0:
                                    if self.rect.x > 85:
                                        self.rect.x -= 2
                                    else:
                                        self.posMove = 1
                                case 1:
                                    if self.rect.x < 1090:
                                        self.rect.x += 2
                                    else:
                                        self.posMove = 0
                            self.switchPos -= 1
                        else:
                            self.isSwitchPos = False
                self.timeLive += 1
            else:
                match self.posMove:
                    case 0:
                        if self.rect.x > -25:
                            self.rect.x -= 3
                        else:
                            self.Health = 250
                            self.timeMove = 0
                            self.isMove = False
                            self.rect.y = -200
                            self.rect.x = -200
                    case 1:
                        if self.rect.x < 1290:
                            self.rect.x += 3
                        else:
                            self.Health = 250
                            self.timeMove = 0
                            self.isMove = False
                            self.rect.y = -200
                            self.rect.x = -200
