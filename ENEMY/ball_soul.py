from random import randint

import pygame

from SCENE import scene_config
from group_config import ball_soul

from BULLET import bullet_enemy
from config_script import createImage

fileBullet = ("ASSETS/SPRITE/BULLET/ENEMY/simple/1.png", "ASSETS/SPRITE/BULLET/ENEMY/simple/2.png")


class Ball_soul(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/1.png")
        self.size: int = 35
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.rect = self.image.get_rect(center=(-300, -300))
        self.add(ball_soul)

        self.Health: int = 100

        match scene_config.DIFFICULTY:
            case 1:
                self.Health: int = 155
            case 2:
                self.Health: int = 225

        self.maxDown: int = randint(105, 170)

        self.Frame: int = 0

        self.TimeShot: int = 0
        self.shootType: int = 0
        self.timeDown: int = 0

        self.timeLeft: int = 0

        self.selectBack: int = randint(0, 1)

        self.isMove: bool = False

        self.timeMove: int = 0
        match scene_config.DIFFICULTY:
            case 0:
                self.timeMoveMax = randint(85, 6465)
            case 1:
                self.timeMoveMax = randint(65, 6065)
            case 2:
                self.timeMoveMax = self.timeMoveMax = randint(20, 5450)
        for i in ball_soul:
            if i.timeMoveMax == self.timeMoveMax:
                match scene_config.DIFFICULTY:
                    case 0:
                        self.timeMoveMax = randint(85, 6465)
                    case 1:
                        self.timeMoveMax = randint(65, 6065)
                    case 2:
                        self.timeMoveMax = self.timeMoveMax = randint(20, 5450)

    def createBullet(self):
        match self.TimeShot:
            case 35:
                bullet_enemy.bulletHell(self.rect.x + 21, self.rect.y + 29, "ASSETS/SPRITE/BULLET/ENEMY/10.png",
                                        self.shootType, 9)
                if self.shootType < 8:
                    self.shootType += 1
                else:
                    self.shootType = 1
                self.TimeShot = 0
            case _:
                self.TimeShot += 1

    def animation(self):
        match self.Frame:
            case 10:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/1.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
            case 20:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/2.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
            case 30:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/3.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
            case 40:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/4.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
            case 50:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/3.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
            case 60:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_SOUL/2.png")
                self.image = pygame.transform.scale(self.image, (self.size, self.size))
                self.Frame = 0

        self.Frame += 1

    def update(self):
        if self.timeMove == self.timeMoveMax * 6:
            if not self.isMove:
                self.rect.x = randint(120, 990)
                self.rect.y = -15
                self.isMove = True
        else:
            self.timeMove += 1
        if self.isMove:
            self.animation()
            self.createBullet()
            if self.timeLeft <= 590:
                self.timeLeft += 1
                if self.timeDown < self.maxDown:
                    self.timeDown += 1
                    self.rect.y += 4
            else:
                match self.selectBack:
                    case 0:
                        if self.rect.x > -20:
                            self.rect.x -= 4
                        else:
                            self.timeMove = 0
                            self.isMove = False
                            self.rect.y = -300
                            self.rect.x = -300
                    case 1:
                        if self.rect.x < 1280:
                            self.rect.x += 4
                        else:
                            self.timeMove = 0
                            self.isMove = False
                            self.rect.y = -300
                            self.rect.x = -300
