from ENEMY import rosa_ball, shota, shutle, ball_soul, magical_girl, card_girl, Eva, Fly_Blood


def createBall():
    return rosa_ball.ball()


def createShota():
    return shota.shota()


def createShutlePatrol():
    return shutle.patrol()


def createBallSoul():
    return ball_soul.Ball_soul()


def createMagicalGirl(type=0):
    match type:
        case 0:
            return magical_girl.type_one()


def createShutleRocket():
    return shutle.rocket()


def createShutleItem():
    return shutle.item_shutle()


def createCardGirl():
    return card_girl.card_girl()


def createEva():
    return Eva.eva()


def createFly():
    return Fly_Blood.fly()
