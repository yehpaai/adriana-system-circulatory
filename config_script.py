import pygame

pygame.mixer.pre_init(44100, 32, 2, 4096)
pygame.init()

WINDOWS_SIZE = (1280, 720)
TITLE: str = "Adriana::System: Circulatory: "

LANGUAGE_SET = None

try:
    lang_file = open(".language_data.langa", "r", encoding="utf-8")
    LANGUAGE_SET = lang_file.read(2)
except FileNotFoundError:
    lang_file = open(".language_data.langa", "w", encoding="utf-8")
    lang_file.write("EN")
    LANGUAGE_SET = "EN"
lang_file.close()

TEXT_LEVEL_EN = ("Kill Count: ", "EXP: ", "Ammo: ", "SCORE: ", "Adriana MAX!", "Crystal Count: ")
TEXT_LEVEL_UA = ("Вбито: ", "ЕКСП: ", "Патрони: ", "РЕКОРД: ", "МАКСИМАЛЬНА Адріана!", "Зібрано Кристалів: ")

TEXT_MENU_EN = ("PLAY", "SCORE", "OPTIONS", "EXIT", "ABOUT")
TEXT_MENU_UA = ("ГРАТИ", "РЕКОРДИ", "НАЛАШТУВАННЯ", "ВИЙТИ", "ПРО")

TEXT_MODE_EN = ("STORY", "SCORE", "MUSIC", "SANDBOX", "SELECT MODE:")
TEXT_MODE_UA = ("ІСТОРІЯ", "РЕКОРД", "МУЗИКАЛЬНИЙ", "ПІСОЧНИЦЯ", "ОБЕРІТЬ РЕЖИМ:")

TEXT_RANK_EN = ("Your Rank:", "Kill Count: ", "Health: ", "Score: ", "Press Any Button (Please)")
TEXT_RANK_UA = ("Твій Ранг:", "Вбито: ", "Рівень Здоров'я: ", "Рекорд: ", "Натисніть Любу Клавішу (Будь-ласка)")

TEXT_DIE_EN = ("CONGRATULATIONS", "YOUR ARE DIE!", "Try Again", "Exit Main Main", "Exit Game", "Time Over")
TEXT_DIE_UA = ("ВІТАЮ", "ТИ ПОМЕР!", "Спробувати Ще Раз", "Вийти В Головне Меню", "Вийти З Гри", "Час Вичерпано")

TEXT_OPTION_EN = ("Other", "Regenerate Game:", "Language:", "Anti-Alaising Text:", "Show FPS:", "EXIT",
                  "Full-screen:")
TEXT_OPTION_UA = ("ІНШІ", "Регенерувати Гру:", "Мова:", "Згладжування Тексту:", "Відображати FPS:", "ВИЙТИ",
                  "Повний Екран: ")

TEXT_OPTION_OTHER_EN = ("Inertia Player:", "Particle:", "Move Back Effect", "Count Back Particle: ", "Show Pointer:",
                        "Rotate Object:", "Animation AD:", "Trash Count: ", "\"AD\":")
TEXT_OPTION_OTHER_UA = ("Інерція Гравця:", "Партікли:", "Рух Задніх Ефектів:", "Кількість Задніх Партіклів: ",
                        "Відображати Вказівник:", "Обертати Об'єкти:", "Анімація Реклами:", "Кількість Сміття: ", "\"Реклама\":")

TEXT_WARNING_EN = ("Requsting Restart Game", "Press E To Continue")
TEXT_WARNING_UA = ("Потрібно Перезагрузити Гру", "Натисніть \"E\" Щоб Продовжити")

TEXT_LOADING_EN = (":TO PRAY!", ":YANDERE KILL", "CREATE PEOPLE")
TEXT_LOADING_UA = (":МОЛИМОСЬ!", ":ЯНДЕРЕ ВБИВАЄ", "НАРОДЖУЄМО ЛЮДЕЙ")

TEXT_NOTFICATION_EN = ("Press Y To open skill tree", "New Achivments")
TEXT_NOTFICATION_UA = ("Для Відкриття Дерево Вмінь: Y", "Нове Досягнення")


TEXT_TASK_EN = ("Kill  Enemy For The Time", "You will hold on?", "Collect  Crystal")
TEXT_TASK_UA = ("Вбийте  Москалів За Відведений Час", "Протримаєшся?", "Зберіть  Кристали")

TEXT_SKILL_EN = ("HELL DAMAGE: 6200", "BULLET AWESOME: 8500", "GOOD REGENERATION: 9500")
TEXT_SKILL_UA = ("КУЛІ ПЕКЛА: 6200", "НЕЗЛАМНІ КУЛІ: 8500", "БОЖА РЕГЕНЕРАЦІЯ: 9500")

sc = pygame.display.set_mode((WINDOWS_SIZE[0], WINDOWS_SIZE[1]), pygame.DOUBLEBUF, pygame.OPENGL)

clock = pygame.time.Clock()


def createImage(filename):
    return pygame.image.load(filename).convert_alpha()
