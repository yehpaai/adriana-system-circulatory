from random import randint

import pygame

import group_config
from SCENE import scene_config
from BULLET import bullet_player
from config_script import createImage
from sfx_compilation import SHOOT_PLAYER_FIRST, SHOOT_PLAYER_SECOND
from key_setup import PLAYER_MOVE, PLAYER_SHOOT


class Player(pygame.sprite.Sprite):
    def __init__(self, x, y, speed, filename):
        pygame.sprite.Sprite.__init__(self)
        self.filename = filename
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, (16, 26))
        self.rect = self.image.get_rect(center=(x, y))
        self.speed: int = speed
        self.EXP: int = 0
        self.MultipleScore: int = 0
        self.Health: int = randint(325, 355)
        self.Ammo: int = randint(550, 850)
        self.damageBullet: int = 3
        self.ExtraDamageBullet: int = 0

        self.isAutoRegen: bool = False
        self.timeRegen: int = 0

        # Force
        self.isInertia: bool = True
        self.ForceUp: int = 0
        self.ForceDown: int = 0
        self.ForceLeft: int = 0
        self.ForceRight: int = 0

        self.timeShoot: int = 5
        self.timeShootMax: int = 11
        self.timePistol: int = 2
        self.timePlasma: int = 2
        self.timeAutoBullet: int = 2
        self.timeLaserBullet: int = 2
        self.timeShotgun: int = 3
        self.timePoison: int = 5

        self.timePoisonCount: int = 5

        self.isControl: bool = True

    def createBullet_Standart(self, rot, pos=0):
        return bullet_player.bullet_standart(self.rect.x + 9, self.rect.y + 10, 10, rot, pos)

    def createBullet_Pistol(self, x, pos):
        return bullet_player.bullet_pistol(x, self.rect.y + 10, pos)

    def createBullet_Plasma(self):
        return bullet_player.bullet_plasma(self.rect.x + 10, self.rect.y + 10)

    def createBullet_Laser(self, x):
        return bullet_player.bullet_pistol(x, self.rect.y + 26, 0, "ASSETS/SPRITE/BULLET/PLAYER/7.png", 26)

    def createBullet_Auto(self):
        return bullet_player.bullet_pos_auto(self.rect.x + 11, self.rect.y + 10)

    def createBulletShotgun(self, pos):
        return bullet_player.bulletShotgun(self.rect.x + 9, self.rect.y + 10, pos)

    def createBulletPison(self):
        return bullet_player.bulletPoison(self.rect.x + 9, self.rect.y + 10)

    def update(self):

        key = pygame.key.get_pressed()

        if key[PLAYER_SHOOT[0]]:
            if self.timeShoot >= self.timeShootMax:
                if self.EXP <= 120 and self.Ammo < 1:
                    SHOOT_PLAYER_SECOND.play()
                if key[pygame.K_SPACE]:
                    self.timeShoot = 0
                    self.createBullet_Standart(0, 1)
                else:
                    if key[PLAYER_SHOOT[1]]:
                        self.timeShoot = 0
                        self.createBullet_Standart(2)
                    elif key[PLAYER_SHOOT[2]]:
                        self.timeShoot = 0
                        self.createBullet_Standart(1)
                    else:
                        self.timeShoot = 0
                        self.createBullet_Standart(0)
            else:
                self.timeShoot += 1
            if self.Ammo > 0:
                if self.EXP >= 250:
                    if self.timePoisonCount > 0:
                        self.Ammo -= 1
                        self.timePoisonCount -= 1
                        self.createBulletPison()
                    else:
                        match self.timePoison:
                            case 120:
                                self.timePoison = 0
                                self.timePoisonCount = 15
                            case _:
                                self.timePoison += 1

                if self.EXP >= 500:
                    match self.timeAutoBullet:
                        case 25:
                            self.Ammo -= 1
                            self.timeAutoBullet = 0
                            self.createBullet_Auto()
                        case _:
                            self.timeAutoBullet += 1
                if self.EXP >= 300:
                    match self.timeLaserBullet:
                        case 2:
                            self.Ammo -= 1
                            self.timeLaserBullet = 0
                            self.createBullet_Laser(self.rect.x + 3)
                            self.createBullet_Laser(self.rect.x + 17)
                        case _:
                            self.timeLaserBullet += 1
                if self.EXP >= 65:
                    match self.timePlasma:
                        case 3:
                            self.Ammo -= 1
                            self.timePlasma = 0
                            self.createBullet_Plasma()
                        case _:
                            self.timePlasma += 1
                if self.EXP >= 120:
                    match self.timePistol:
                        case 7:
                            SHOOT_PLAYER_FIRST.play()
                            self.Ammo -= 1
                            if key[PLAYER_SHOOT[2]]:
                                self.timePistol = 0
                                self.createBullet_Pistol(self.rect.x + 21, 1)
                            elif key[PLAYER_SHOOT[1]]:
                                self.timePistol = 0
                                self.createBullet_Pistol(self.rect.x + 8, 2)
                            else:
                                self.timePistol = 0
                                self.createBullet_Pistol(self.rect.x + 3, 0)
                                self.createBullet_Pistol(self.rect.x + 17, 0)
                        case _:
                            self.timePistol += 1
                if self.EXP >= 165:
                    match self.timeShotgun:
                        case 10:
                            self.Ammo -= 2
                            self.timeShotgun = 0
                            for i in range(3):
                                self.createBulletShotgun(i)
                        case _:
                            self.timeShotgun += 1
        match self.isControl:
            case True:
                match self.isInertia:
                    case True:
                        if key[PLAYER_MOVE[0]] and self.rect.y > 20:
                            if self.ForceUp < self.speed:
                                self.ForceUp += 0.5
                            self.rect.y -= self.ForceUp
                            match scene_config.isMoveBackEffect:
                                case True:
                                    for i in group_config.back_effect_group:
                                        if self.rect.y > 90:
                                            i.rect.y += self.ForceUp - 1
                        else:
                            if self.ForceUp > 0 and self.rect.y > 20:
                                self.ForceUp -= 1
                                self.rect.y -= self.ForceUp
                                match scene_config.isMoveBackEffect:
                                    case True:
                                        for i in group_config.back_effect_group:
                                            if self.rect.y > 90:
                                                i.rect.y += self.ForceUp - 1
                            else:
                                self.ForceUp = 0
                        if key[PLAYER_MOVE[1]] and self.rect.y < 665:
                            if self.ForceDown < self.speed:
                                self.ForceDown += 0.5
                            self.rect.y += self.ForceDown
                            match scene_config.isMoveBackEffect:
                                case True:
                                    for i in group_config.back_effect_group:
                                        if self.rect.y < 580:
                                            i.rect.y -= self.ForceDown - 1
                        else:
                            if self.ForceDown > 0 and self.rect.y < 665:
                                self.ForceDown -= 1
                                self.rect.y += self.ForceDown
                                match scene_config.isMoveBackEffect:
                                    case True:
                                        for i in group_config.back_effect_group:
                                            if self.rect.y < 655:
                                                i.rect.y -= self.ForceDown - 1
                            else:
                                self.ForceDown = 0
                        if key[PLAYER_MOVE[2]] and self.rect.x > 25:
                            if self.ForceLeft < self.speed:
                                self.ForceLeft += 0.5
                            self.rect.x -= self.ForceLeft
                        else:
                            if self.ForceLeft > 0 and self.rect.x > 25:
                                self.ForceLeft -= 1
                                self.rect.x -= self.ForceLeft
                            else:
                                self.ForceLeft = 0
                        if key[PLAYER_MOVE[3]] and self.rect.x < 1225:
                            if self.ForceRight < self.speed:
                                self.ForceRight += 0.5
                            self.rect.x += self.ForceRight
                        else:
                            if self.ForceRight > 0 and self.rect.x < 1225:
                                self.ForceRight -= 1
                                self.rect.x += self.ForceRight
                            else:
                                self.ForceRight = 0
                    case False:
                        if key[pygame.K_LSHIFT]:
                            self.speed = 5
                        else:
                            self.speed = 8
                        if key[PLAYER_MOVE[0]] and self.rect.y > 20:
                            self.rect.y -= self.speed
                            match scene_config.isMoveBackEffect:
                                case True:
                                    for i in group_config.back_effect_group:
                                        i.rect.y += self.speed
                        if key[PLAYER_MOVE[1]] and self.rect.y < 660:
                            self.rect.y += self.speed
                            match scene_config.isMoveBackEffect:
                                case True:
                                    for i in group_config.back_effect_group:
                                        i.rect.y -= self.speed
                        if key[PLAYER_MOVE[2]] and self.rect.x > 25:
                            self.rect.x -= self.speed
                        if key[PLAYER_MOVE[3]] and self.rect.x < 1225:
                            self.rect.x += self.speed

        if self.isAutoRegen:
            if self.Health <= 200:
                match self.timeRegen:
                    case 900:
                        self.timeRegen = 0
                        self.Health += 25
                    case _:
                        self.timeRegen += 1

        if self.EXP >= 45:
            self.timeShootMax = 7
        if self.EXP >= 55:
            self.damageBullet = 3 + self.ExtraDamageBullet
        if self.EXP >= 65:
            scene_config.AngleSpeed = 2
            self.damageBullet = 5 + self.ExtraDamageBullet
        if self.EXP >= 90:
            self.damageBullet = 7 + self.ExtraDamageBullet
        if self.EXP >= 135:
            scene_config.AngleSpeed = 3
            self.damageBullet = 8 + self.ExtraDamageBullet
        if self.EXP >= 145:
            scene_config.AngleSpeed = 4
            self.damageBullet = 11 + self.ExtraDamageBullet
        if self.EXP >= 225:
            scene_config.AngleSpeed = 5
            self.damageBullet = 13 + self.ExtraDamageBullet
        if self.EXP >= 245:
            self.damageBullet = 16 + self.ExtraDamageBullet
        if self.EXP >= 295:
            scene_config.AngleSpeed = 6
            self.damageBullet = 19 + self.ExtraDamageBullet
        if self.EXP >= 315:
            self.damageBullet = 20 + self.ExtraDamageBullet
        if self.EXP >= 345:
            scene_config.AngleSpeed = 7
            self.damageBullet = 23 + self.ExtraDamageBullet
        if self.EXP >= 395:
            self.damageBullet = 27 + self.ExtraDamageBullet
        if self.EXP >= 415:
            self.damageBullet = 31 + self.ExtraDamageBullet
        if self.EXP >= 445:
            self.damageBullet = 35 + self.ExtraDamageBullet
        if self.EXP <= 0:
            self.timeShootMax = 19
