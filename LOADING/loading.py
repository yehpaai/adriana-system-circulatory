from random import randint

import pygame.transform

from config_script import *
from SCENE import scene_config, scene_game
from font_config import LoadingFont
from ENEMY import create_enemy
from group_config import back_effect_group


def loading():
    FPS: int = 60

    TEXT_SET = ("", "", "")

    match LANGUAGE_SET:
        case "UA":
            TEXT_SET = (TEXT_LOADING_UA[0], TEXT_LOADING_UA[1], TEXT_LOADING_UA[2])
        case "EN":
            TEXT_SET = (TEXT_LOADING_EN[0], TEXT_LOADING_EN[1], TEXT_LOADING_EN[2])

    selectLoading: int = randint(0, 2)

    LoadingText = LoadingFont.render(TEXT_SET[0], scene_config.AA_TEXT, (255, 255, 255))

    match selectLoading:
        case 1:
            LoadingText = LoadingFont.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 255))
        case 2:
            LoadingText = LoadingFont.render(TEXT_SET[2], scene_config.AA_TEXT, (255, 255, 255))
    LoadingTextRect = LoadingText.get_rect(center=(980, 655))

    LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/4.png")
    LoadingIcon = pygame.transform.scale(LoadingIcon, (63, 63))

    angleImage = 0
    angleSpeed = randint(1, 4)

    ImageLoad = createImage("ASSETS/SPRITE/UI/LOADING/2.png")
    ImageLoad = pygame.transform.scale(ImageLoad, (328, 328))
    match selectLoading:
        case 1:
            ImageLoad = createImage("ASSETS/SPRITE/UI/LOADING/1.png")
            ImageLoad = pygame.transform.scale(ImageLoad, (328, 328))
        case 2:
            ImageLoad = createImage("ASSETS/SPRITE/UI/LOADING/6.png")
            ImageLoad = pygame.transform.scale(ImageLoad, (328, 328))
    ImageLoadRect = ImageLoad.get_rect(center=(640, 545))

    timeWait: int = 0

    match selectLoading:
        case 1:
            LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/5.png")
            LoadingIcon = pygame.transform.scale(LoadingIcon, (69, 69))
        case 2:
            LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/7.png")
            LoadingIcon = pygame.transform.scale(LoadingIcon, (69, 69))
    LoadingIconRect = LoadingIcon.get_rect(center=(1150, 650))

    while 1:
        match timeWait:
            case 15:
                timeWait += 1
                for i in range(250):
                    for b in back_effect_group:
                        scene_game.createEffect = 0
                        b.kill()
            case 75:
                timeWait += 1
                match scene_config.isMoveBackEffect:
                    case True:
                        scene_config.TimeCountLevelEffect = randint(1505, 2550)
                        for i in range(scene_config.TimeCountLevelEffect * 2):
                            scene_game.createEffectLevel()
                            scene_game.scene0_draw()
                            scene_game.group_config.back_effect_group.update()
                    case False:
                        for i in range(scene_config.TimeCountLevelEffect * 2):
                            scene_game.createEffectLevel()
                            scene_game.scene0_draw()
                            scene_game.group_config.back_effect_group.update()
            case 95:
                timeWait += 1
                for i in range(3):
                    scene_game.scene0_update()
            case 100:
                for i in range(17):
                    create_enemy.createFly()
                for i in range(40):
                    create_enemy.createBall()
                if scene_game.LEVEL >= 1:
                    for i in range(30):
                        create_enemy.createShota()
                if scene_game.LEVEL >= 2:
                    for i in range(35):
                        create_enemy.createShutlePatrol()
                        create_enemy.createBallSoul()
                if scene_game.LEVEL >= 3:
                    for i in range(45):
                        create_enemy.createMagicalGirl()
                if scene_game.LEVEL >= 4:
                    for i in range(15):
                        create_enemy.createShutleItem()
                    for i in range(10):
                        create_enemy.createShutleRocket()
                if scene_game.LEVEL >= 5:
                    for i in range(25):
                        create_enemy.createEva()
                        create_enemy.createCardGirl()
                timeWait += 1
            case 155:
                match scene_config.GameMode:
                    case 0:
                        scene_config.switch_scene(scene_game.scene0)
                return 0
            case _:
                timeWait += 1

        match selectLoading:
            case 2:
                LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/7.png")
                LoadingIcon = pygame.transform.scale(LoadingIcon, (69, 69))
                LoadingIconRect = LoadingIcon.get_rect(center=(1185, 645))
                if LANGUAGE_SET == "EN":
                    LoadingIconRect = LoadingIcon.get_rect(center=(1130, 650))
            case 1:
                LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/5.png")
                LoadingIcon = pygame.transform.scale(LoadingIcon, (69, 69))
                LoadingIconRect = LoadingIcon.get_rect(center=(1155, 650))
            case 0:
                LoadingIcon = createImage("ASSETS/SPRITE/UI/LOADING/4.png")
                LoadingIcon = pygame.transform.scale(LoadingIcon, (63, 63))
                LoadingIconRect = LoadingIcon.get_rect(center=(1130, 650))
        angleImage += angleSpeed
        LoadingIcon = pygame.transform.rotate(LoadingIcon, angleImage)

        sc.fill((0, 0, 0))
        sc.blit(LoadingText, LoadingTextRect)
        sc.blit(LoadingIcon, LoadingIconRect)
        sc.blit(ImageLoad, ImageLoadRect)
        pygame.display.update()
        clock.tick(FPS)
