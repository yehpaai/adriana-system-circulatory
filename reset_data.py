from random import randint
from pygame import transform

import group_config
from SCENE import scene_config, scene_game
from config_script import WINDOWS_SIZE, createImage
from SKILLTREE import SkillTree


def reset():
    for ball_rosa in group_config.ball_rosa_group:
        ball_rosa.kill()
    for shota in group_config.shota_group:
        shota.kill()
    for shutle in group_config.shutle_group:
        shutle.kill()
    for soulBall in group_config.ball_soul:
        soulBall.kill()
    for magical in group_config.magical_girl_group:
        magical.kill()
    for bullet_sim in group_config.simple_bullet:
        bullet_sim.kill()
    for p_b in group_config.player_bullet:
        p_b.kill()
    for ab in group_config.anyBullet:
        ab.kill()
    for pistol in group_config.pistol_bullet:
        pistol.kill()
    for crystal in group_config.item_group:
        crystal.kill()
    for effect in group_config.back_effect_group:
        effect.kill()
    for skill in SkillTree.slot_group:
        skill.image = createImage("ASSETS/SPRITE/UI/SkillTree/1.png")
        skill.image = transform.scale(skill.image, (87, 82))
        skill.isBuy = False

    scene_config.isDrawPlayer = True
    scene_config.Score = 0
    scene_config.CRYSTAL_COUNT = 0
    scene_config.KILL_COUNT = 0
    scene_game.LEVEL = 0
    scene_config.LEVEL_TASK = 2
    scene_config.CRYSTAL_MAX = 0
    scene_config.p.Ammo = randint(550, 900)
    scene_config.p.EXP = 0
    scene_config.p.Health = randint(320, 370)
    scene_config.p.rect.x = WINDOWS_SIZE[0]//2
    scene_config.p.rect.y = WINDOWS_SIZE[0] // 2
    scene_config.p.isControl = True
