from random import randint

import pygame
from pygame import BLEND_RGB_ADD

from glow import createCircle
from group_config import effect_group, back_effect_group, fill_group, bodies_effect_group
from config_script import WINDOWS_SIZE, createImage, sc
from SCENE import scene_config
import time


class effect_bodies(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size=24, isMetal=False):
        pygame.sprite.Sprite.__init__(self)
        self.filename = filename
        self.size = size
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(bodies_effect_group)

        self.timeLife: int = 0
        self.pos: int = randint(0, 5)

        self.Alpha: int = randint(140, 350)

        self.timePos: int = 50

        self.isMetal: bool = isMetal

        self.x = x
        self.y = y
        self.angle = 0

    def update(self):
        if self.timeLife > 165:
            if self.Alpha <= 1:
                self.kill()
            else:
                self.Alpha -= 2
        else:
            self.timeLife += 1

        match self.isMetal:
            case True:
                match self.timePos:
                    case 125:
                        self.timePos = 0
                        self.pos = randint(0, 5)
                    case _:
                        self.timePos += 1

        match self.pos:
            case 0:
                self.y -= 1
                self.x -= 1
            case 1:
                self.y += 1
                self.x += 1
            case 2:
                self.y += 1
                self.x -= 1
            case 3:
                self.y -= 1
                self.x += 1
            case 4:
                self.y -= 1
            case 5:
                self.y += 1
        if scene_config.isRotateObject:
            self.image = createImage(self.filename)
            self.image = pygame.transform.scale(self.image, (self.size, self.size))
            self.angle += 4
            self.image = pygame.transform.rotate(self.image, self.angle)
        self.rect = self.image.get_rect(center=(self.x, self.y))
        self.image.set_alpha(self.Alpha)


class effect_simple(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, pos=0, size=64):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, (size, size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(effect_group)

        match pos:
            case 1:
                self.image = pygame.transform.flip(self.image, True, False)
            case 2:
                self.image = pygame.transform.flip(self.image, False, True)

        self.timeKill: int = 0

    def update(self):
        match self.timeKill:
            case 4:
                self.kill()
            case _:
                self.timeKill += 1


class effect_anim(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, frame=6, size=64, type_effect=0):
        pygame.sprite.Sprite.__init__(self)
        self.image = createImage(filename[0])
        self.image = pygame.transform.scale(self.image, (size, size))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(effect_group)
        self.filename: [] = filename

        self.type = frame

        self.size: int = size
        self.Frame: int = 0
        self.Alpha: int = 250

        self.radius: int = 1
        self.color = [255, 255, 255]

        self.type_effect: int = type_effect

    def update(self):
        if self.type_effect == 1:
            self.radius += 20
            if self.radius <= 145:
                sc.blit(createCircle(self.radius, self.color),
                        (self.rect.centerx - self.radius, self.rect.centery - self.radius),
                        special_flags=BLEND_RGB_ADD)
            self.color[0] -= 15
            self.color[1] -= 20
            self.color[2] -= 20
        match self.type:
            case 6:
                match self.Frame:
                    case 6:
                        self.image = createImage(self.filename[1])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 14:
                        self.image = createImage(self.filename[2])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 25:
                        self.image = createImage(self.filename[3])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 30:
                        self.image = createImage(self.filename[4])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 35:
                        self.image = createImage(self.filename[5])
                        self.image = pygame.transform.scale(self.image, (self.size + 1, self.size + 1))
                    case 39:
                        self.kill()
                self.Frame += 1
                self.image.set_alpha(self.Alpha)
                self.Alpha -= 19
            case 3:
                match self.Frame:
                    case 5:
                        self.image = createImage(self.filename[1])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 9:
                        self.image = createImage(self.filename[2])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 10:
                        self.kill()
                self.Frame += 1
                self.image.set_alpha(self.Alpha)
                self.Alpha -= 5
            case 4:
                match self.Frame:
                    case 6:
                        self.image = createImage(self.filename[1])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 11:
                        self.image = createImage(self.filename[2])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 16:
                        self.image = createImage(self.filename[3])
                        self.image = pygame.transform.scale(self.image, (self.size, self.size))
                    case 23:
                        self.kill()
                self.Frame += 1
                self.image.set_alpha(self.Alpha)
                self.Alpha -= 5


class effect_take(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size=(24, 24)):
        super().__init__()
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(effect_group)

        self.alpha: int = 300

        self.posMove: int = randint(0, 2)

        self.lifeTime: int = 0

    def update(self):
        if self.lifeTime <= 25:
            match self.posMove:
                case 0:
                    self.rect.y -= 4
                case 1:
                    self.rect.y -= 4
                    self.rect.x -= 2
                case 2:
                    self.rect.y -= 4
                    self.rect.x += 2
            self.alpha -= 6
            self.image.set_alpha(self.alpha)
        else:
            self.kill()


class effectLevel(pygame.sprite.Sprite):
    def __init__(self, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/EFFECT/LEVEL/1/1.png")
        self.image = pygame.transform.scale(self.image, (29, 29))
        self.add(back_effect_group)
        self.Frame = 0

        self.speed = randint(1, 5)

        posSelect = randint(0, 1)
        self.pos = posSelect

        self.Alpha = randint(1, 75)
        self.image.set_alpha(self.Alpha)

        self.xMinus = randint(15, 575)

        match self.pos:
            case 0:
                self.rect = self.image.get_rect(center=(-15, y))
                self.rect.x -= self.xMinus
            case 1:
                self.image = pygame.transform.flip(self.image, True, False)
                self.rect = self.image.get_rect(center=(1280, y))
                self.rect.x += self.xMinus

    def update(self):
        match self.pos:
            case 0:
                if self.rect.x <= 1270:
                    self.rect.x += self.speed
                else:
                    self.pos = 1
            case 1:
                if self.rect.x >= -20:
                    self.rect.x -= self.speed
                else:
                    self.pos = 0


class colorFill(pygame.sprite.Sprite):
    def __init__(self, filename, force=10):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(filename).convert()
        self.image = pygame.transform.scale(self.image, WINDOWS_SIZE)
        self.rect = self.image.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))
        self.add(fill_group)

        self.Alpha: int = 250

        self.force = force

        self.prev_time = time.time()

    def update(self):

        dt = time.time() - self.prev_time
        dt *= 60
        self.prev_time = time.time()

        if self.Alpha > 2:
            self.Alpha -= self.force * dt
            self.image.set_alpha(self.Alpha)
        else:
            self.remove()
            self.kill()
