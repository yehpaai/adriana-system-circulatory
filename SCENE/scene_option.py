import pygame

import change_op
import take_screenshot
from SCENE import scene_config, scene_select_setings, scene_menu
import config_script
from font_config import TextOption
from sfx_compilation import SELECT_MENU, ENTER_SELECT
import reset_data
from group_config import fill_group, notfication_group

isRegen = False
RegenFrame: int = 0

TEXT_SET = ("", "", "", "", "", "", "", '')

fileRegen = "ASSETS/SPRITE/UI/BUTTON/ButtonGenUKR/1.png"

match config_script.LANGUAGE_SET:
    case "UA":
        TEXT_SET = (config_script.TEXT_OPTION_UA[0], config_script.TEXT_OPTION_UA[1], config_script.TEXT_OPTION_UA[2],
                    config_script.TEXT_OPTION_UA[3], config_script.TEXT_OPTION_UA[4], config_script.TEXT_OPTION_UA[5],
                    config_script.TEXT_WARNING_UA[0], config_script.TEXT_WARNING_UA[1], config_script.TEXT_OPTION_UA[6])
    case "EN":
        fileRegen = "ASSETS/SPRITE/UI/BUTTON/ButtonGen/1.png"
        TEXT_SET = (config_script.TEXT_OPTION_EN[0], config_script.TEXT_OPTION_EN[1], config_script.TEXT_OPTION_EN[2],
                    config_script.TEXT_OPTION_EN[3], config_script.TEXT_OPTION_EN[4], config_script.TEXT_OPTION_EN[5],
                    config_script.TEXT_WARNING_EN[0], config_script.TEXT_WARNING_EN[1], config_script.TEXT_OPTION_EN[6])

RegenImage = pygame.image.load(fileRegen).convert_alpha()
RegenImage = pygame.transform.scale(RegenImage, (44, 43))
RegenImageRect = RegenImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 130, 455))
angleRegen = 0

sizeWindow = 1
WindowImage = pygame.image.load("ASSETS/SPRITE/UI/WINDOW.png").convert_alpha()
WindowImage = pygame.transform.scale(WindowImage, (sizeWindow, sizeWindow))
WindowImageRect = WindowImage.get_rect(center=(scene_config.WINDOWS_SIZE[0] // 2, scene_config.WINDOWS_SIZE[1] // 2))
FrameWindow = 0

windowLangText = TextOption.render(TEXT_SET[6], scene_config.AA_TEXT, (250, 250, 250))
windowLangRect = windowLangText.get_rect(
    center=(config_script.WINDOWS_SIZE[0] // 2, config_script.WINDOWS_SIZE[1] // 2 - 30))

TextContinueLang = TextOption.render(TEXT_SET[7], scene_config.AA_TEXT, (255, 255, 255))
TextContinueLangRect = TextContinueLang.get_rect(
    center=(config_script.WINDOWS_SIZE[0] // 2 + 10, config_script.WINDOWS_SIZE[1] // 2 + 90))

isFull_screen: bool = False


def option():
    global isRegen, FrameWindow, sizeWindow, WindowImage, WindowImageRect, TextContinueLangRect, isFull_screen
    running: bool = True
    FPS: int = 60

    select: int = 0

    ExitText = TextOption.render(TEXT_SET[5], scene_config.AA_TEXT, (255, 255, 255))
    ExitTextRect = ExitText.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, 570))

    RegenText = TextOption.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 255))
    RegenTextRect = RegenText.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, 455))

    FullsceenText = TextOption.render(TEXT_SET[8], scene_config.AA_TEXT, (255, 255, 255))
    FullsceenTextRect = FullsceenText.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, 305))

    AAText = TextOption.render(TEXT_SET[3], scene_config.AA_TEXT, (255, 255, 255))
    AATextRect = AAText.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, 355))

    LangText = TextOption.render(TEXT_SET[2], scene_config.AA_TEXT, (255, 255, 255))
    LangTextRect = AAText.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 100, 245))

    FPSText = TextOption.render(TEXT_SET[4], scene_config.AA_TEXT, (255, 255, 255))
    FPSTextRect = AAText.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 30, 405))

    arrowImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ARROW/1.png").convert_alpha()
    arrowImage = pygame.transform.scale(arrowImage, (32, 32))
    arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 65, 245))

    AATextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
    AATextBoolImage = pygame.transform.scale(AATextBoolImage, (64, 64))
    AATextBoolRect = AATextBoolImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 145, 325))

    FSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
    FSTextBoolImage = pygame.transform.scale(FSTextBoolImage, (64, 64))
    FSTextBoolRect = FSTextBoolImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 127, 305))

    FPSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
    FPSTextBoolImage = pygame.transform.scale(FPSTextBoolImage, (64, 64))
    FPSTextBoolRect = FPSTextBoolImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 145, 405))

    LanguageImage = pygame.image.load("ASSETS/SPRITE/UI/FLAG/UK.png").convert_alpha()
    LanguageImage = pygame.transform.scale(LanguageImage, (39, 34))
    LanguageImageRect = LanguageImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 60, 245))

    match config_script.LANGUAGE_SET:
        case "EN":
            TextContinueLangRect = TextContinueLang.get_rect(
                center=(config_script.WINDOWS_SIZE[0] // 2, config_script.WINDOWS_SIZE[1] // 2 + 60))
            LanguageImage = pygame.image.load("ASSETS/SPRITE/UI/FLAG/US.png").convert_alpha()
            LanguageImage = pygame.transform.scale(LanguageImage, (39, 34))
            LanguageImageRect = LanguageImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 90, 245))
            FPSTextRect = AAText.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 50, 405))
            LangTextRect = AAText.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 50, 245))
            FPSTextBoolRect = FPSTextBoolImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 100, 405))
            arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 95, 245))
            AATextBoolRect = AATextBoolImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 145, 355))
            FullsceenTextRect = FullsceenText.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, 304))
        case "UA":
            AATextBoolRect = AATextBoolImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 175, 355))

    isAnimWindow: bool = False
    isKey: bool = True

    ColorSelect = (0, 0, 200)

    yRectSelect: int = 245

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 16)

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take_screenshot.take()
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    ENTER_SELECT.play()
                    match select:
                        case 0:
                            match isAnimWindow:
                                case True:
                                    running = False
                                    scene_config.switch_scene(None)
                            isKey = False
                            isAnimWindow = True
                            change_op.change()
                        case 1:
                            match isFull_screen:
                                case False:
                                    isFull_screen = True
                                    scene_config.sc = pygame.display.set_mode((scene_config.WINDOWS_SIZE[0], scene_config.WINDOWS_SIZE[1]),
                                                                              pygame.FULLSCREEN, pygame.OPENGL)
                                case True:
                                    isFull_screen = False
                                    scene_config.sc = pygame.display.set_mode(
                                        (scene_config.WINDOWS_SIZE[0], scene_config.WINDOWS_SIZE[1]), pygame.DOUBLEBUF, pygame.OPENGL)
                        case 2:
                            match scene_config.AA_TEXT:
                                case False:
                                    scene_config.AA_TEXT = True
                                case True:
                                    scene_config.AA_TEXT = False
                        case 3:
                            match scene_config.FPS_SHOW:
                                case False:
                                    scene_config.FPS_SHOW = True
                                case True:
                                    scene_config.FPS_SHOW = False
                        case 4:
                            isRegen = True
                        case 6:
                            scene_config.switch_scene(scene_select_setings.scene)
                            running = False
                if e.key == pygame.K_DOWN and isKey or e.key == pygame.K_s and isKey:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 2
                        case 2:
                            select = 3
                        case 3:
                            select = 4
                        case 4:
                            select = 6
                        case 6:
                            select = 0
                if e.key == pygame.K_UP and isKey or e.key == pygame.K_w and isKey:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 6
                        case 1:
                            select = 0
                        case 2:
                            select = 1
                        case 3:
                            select = 2
                        case 4:
                            select = 3
                        case 6:
                            select = 4

        match select:
            case 0:
                ColorSelect = (0, 0, 200)
                yRectSelect = 235
                LangText.set_alpha(300)
                FullsceenText.set_alpha(40)
                AAText.set_alpha(40)
                FPSText.set_alpha(40)
                RegenText.set_alpha(40)
                ExitText.set_alpha(40)
                match scene_config.LANGUAGE_SET:
                    case "UA":
                        arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 65, 245))
                    case "EN":
                        arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 85, 245))
            case 1:
                LangText.set_alpha(40)
                FullsceenText.set_alpha(300)
                AAText.set_alpha(40)
                FPSText.set_alpha(40)
                ExitText.set_alpha(40)
                RegenText.set_alpha(40)
                yRectSelect = 295
                match scene_config.LANGUAGE_SET:
                    case "UA":
                        arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 145, 305))
                    case "EN":
                        arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 95, 305))
            case 2:
                LangText.set_alpha(40)
                FullsceenText.set_alpha(40)
                AAText.set_alpha(300)
                FPSText.set_alpha(40)
                ExitText.set_alpha(40)
                RegenText.set_alpha(40)
                yRectSelect = 345
                match scene_config.LANGUAGE_SET:
                    case "UA":
                        arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 160, 355))
                    case "EN":
                        arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 140, 355))
            case 3:
                yRectSelect = 395
                LangText.set_alpha(40)
                FullsceenText.set_alpha(40)
                AAText.set_alpha(40)
                FPSText.set_alpha(300)
                ExitText.set_alpha(40)
                RegenText.set_alpha(40)
                match scene_config.LANGUAGE_SET:
                    case "UA":
                        arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 135, 405))
                    case "EN":
                        arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 95, 405))
            case 4:
                LangText.set_alpha(40)
                FullsceenText.set_alpha(40)
                AAText.set_alpha(40)
                FPSText.set_alpha(40)
                ExitText.set_alpha(40)
                RegenText.set_alpha(300)
                yRectSelect = 445
                arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 135, 455))
            case 6:
                ColorSelect = (0, 145, 0)
                LangText.set_alpha(40)
                FullsceenText.set_alpha(40)
                AAText.set_alpha(40)
                FPSText.set_alpha(40)
                ExitText.set_alpha(300)
                RegenText.set_alpha(40)
                yRectSelect = 560
                arrowRect = arrowImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 - 70, 570))

        match isRegen:
            case True:
                regen()

        match scene_config.AA_TEXT:
            case True:
                AATextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png").convert_alpha()
                AATextBoolImage.set_alpha(300)
                AATextBoolImage = pygame.transform.scale(AATextBoolImage, (64, 64))
            case False:
                AATextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
                AATextBoolImage.set_alpha(100)
                AATextBoolImage = pygame.transform.scale(AATextBoolImage, (64, 64))
        match scene_config.FPS_SHOW:
            case True:
                FPSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png").convert_alpha()
                FPSTextBoolImage.set_alpha(300)
                FPSTextBoolImage = pygame.transform.scale(FPSTextBoolImage, (64, 64))
            case False:
                FPSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
                FPSTextBoolImage.set_alpha(100)
                FPSTextBoolImage = pygame.transform.scale(FPSTextBoolImage, (64, 64))
        match isFull_screen:
            case True:
                FSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/9.png").convert_alpha()
                FSTextBoolImage = pygame.transform.scale(FSTextBoolImage, (64, 64))
                FSTextBoolImage.set_alpha(300)
            case False:
                FSTextBoolImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ButtonBool/1.png").convert_alpha()
                FSTextBoolImage = pygame.transform.scale(FSTextBoolImage, (64, 64))
                FSTextBoolImage.set_alpha(100)

        config_script.sc.fill((0, 0, 0))
        pygame.draw.rect(config_script.sc, ColorSelect, (0, yRectSelect, config_script.WINDOWS_SIZE[0], 22))
        config_script.sc.blit(RegenText, RegenTextRect)
        config_script.sc.blit(RegenImage, RegenImageRect)
        config_script.sc.blit(LangText, LangTextRect)
        config_script.sc.blit(AAText, AATextRect)
        config_script.sc.blit(FPSText, FPSTextRect)
        config_script.sc.blit(ExitText, ExitTextRect)
        config_script.sc.blit(AATextBoolImage, AATextBoolRect)
        config_script.sc.blit(LanguageImage, LanguageImageRect)
        config_script.sc.blit(FPSTextBoolImage, FPSTextBoolRect)
        config_script.sc.blit(FullsceenText, FullsceenTextRect)
        config_script.sc.blit(FSTextBoolImage, FSTextBoolRect)
        config_script.sc.blit(arrowImage, arrowRect)
        config_script.sc.blit(scene_menu.EKeyImage, scene_menu.EKeyImageRect)
        config_script.sc.blit(scene_menu.TextOK, scene_menu.TextOKRect)
        match isAnimWindow:
            case True:
                if sizeWindow <= 525:
                    sizeWindow += 95
                    WindowImage = pygame.image.load("ASSETS/SPRITE/UI/WINDOW.png").convert_alpha()
                    WindowImage = pygame.transform.scale(WindowImage, (sizeWindow + 90, sizeWindow))
                    WindowImageRect = WindowImage.get_rect(
                        center=(scene_config.WINDOWS_SIZE[0] // 2 - 10, scene_config.WINDOWS_SIZE[1] // 2))
                    WindowImage.set_alpha(250)
                    scene_config.sc.blit(WindowImage, WindowImageRect)
                else:
                    scene_config.sc.blit(WindowImage, WindowImageRect)
                    scene_config.sc.blit(windowLangText, windowLangRect)
                    scene_config.sc.blit(TextContinueLang, TextContinueLangRect)
        notfication_group.draw(config_script.sc)
        for n in notfication_group:
            config_script.sc.blit(n.text, n.rectTxt)
        fill_group.draw(config_script.sc)
        pygame.display.update()
        config_script.clock.tick(FPS)
        fill_group.update()
        notfication_group.update()


def regen():
    global fileRegen, RegenImage, isRegen, RegenFrame, angleRegen, RegenImageRect
    match isRegen:
        case True:
            if RegenFrame <= 30:
                reset_data.reset()
                angleRegen += 15
            else:
                angleRegen = 0
                RegenFrame = 0
                isRegen = False
            RegenImage = pygame.image.load(fileRegen).convert_alpha()
            RegenImage = pygame.transform.scale(RegenImage, (44, 43))
            RegenImage = pygame.transform.rotate(RegenImage, angleRegen)
            RegenImageRect = RegenImage.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2 + 130, 455))
            RegenFrame += 1
