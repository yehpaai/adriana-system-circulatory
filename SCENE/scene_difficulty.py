import pygame
from SCENE import scene_config, scene_SelectMode, scene_menu
from LOADING import loading
from config_script import sc, clock, createImage, LANGUAGE_SET, WINDOWS_SIZE
from font_config import NovelTextName, FontKILL, FinishFont
from group_config import fill_group
from sfx_compilation import ENTER_SELECT, SELECT_MENU


def difficulty():
    running: bool = True
    FPS: int = 60

    TextTitle = FinishFont.render("Оберіть Складність", scene_config.AA_TEXT, (255, 255, 255))

    match LANGUAGE_SET:
        case "EN":
            TextTitle = FinishFont.render("Select Difficulty", scene_config.AA_TEXT, (255, 255, 255))

    TEXT_SET = ("ЛЕГКО", "НОРМАЛЬНО", "СКЛАДНО!")

    match LANGUAGE_SET:
        case "EN":
            TEXT_SET = ("EASE", "NORMAL", "HARD!")

    TextEasy = NovelTextName.render(TEXT_SET[0], scene_config.AA_TEXT, (0, 245, 0))
    TextNormal = NovelTextName.render(TEXT_SET[1], scene_config.AA_TEXT, (255, 255, 0))
    TextHard = NovelTextName.render(TEXT_SET[2], scene_config.AA_TEXT, (245, 0, 0))

    EasyImage = createImage("ASSETS/SPRITE/UI/ICON/DIFFICULTY/1.png")
    EasyImage = pygame.transform.scale(EasyImage, (256, 256))

    NormalImage = createImage("ASSETS/SPRITE/UI/ICON/DIFFICULTY/2.png")
    NormalImage = pygame.transform.scale(NormalImage, (258, 258))

    HardImage = createImage("ASSETS/SPRITE/UI/ICON/DIFFICULTY/3.png")
    HardImage = pygame.transform.scale(HardImage, (256, 256))

    SelectImage = createImage("ASSETS/SPRITE/UI/SkillTree/2.png")
    SelectImage = pygame.transform.scale(SelectImage, (86, 60))
    SelectImageRect = SelectImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 460, WINDOWS_SIZE[1] // 2 + 105))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1082, 680))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1130, 680))

    select: int = 0

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 17)

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_RIGHT or e.key == pygame.K_d:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 1
                        case 1:
                            select = 2
                        case 2:
                            select = 0
                if e.key == pygame.K_LEFT or e.key == pygame.K_a:
                    SELECT_MENU.play()
                    match select:
                        case 0:
                            select = 2
                        case 1:
                            select = 0
                        case 2:
                            select = 1
                if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                    ENTER_SELECT.play()
                    match select:
                        case 0:
                            scene_config.DIFFICULTY = 0
                            running = False
                            scene_config.switch_scene(loading.loading)
                        case 1:
                            scene_config.DIFFICULTY = 1
                            running = False
                            scene_config.switch_scene(loading.loading)
                        case 2:
                            scene_config.DIFFICULTY = 2
                            running = False
                            scene_config.switch_scene(loading.loading)
                if e.key == pygame.K_q:
                    running = False
                    scene_config.switch_scene(scene_SelectMode.scene_select)

        match select:
            case 0:
                SelectImageRect = SelectImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 460, WINDOWS_SIZE[1] // 2 + 105))
            case 1:
                SelectImageRect = SelectImage.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 105))
            case 2:
                SelectImageRect = SelectImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 460, WINDOWS_SIZE[1] // 2 + 105))

        sc.fill((0, 0, 0))
        sc.blit(TextTitle, TextTitle.get_rect(center=(WINDOWS_SIZE[0] // 2, 90)))
        sc.blit(TextEasy, TextEasy.get_rect(center=(WINDOWS_SIZE[0] // 2 - 460, WINDOWS_SIZE[1] // 2 + 105)))
        sc.blit(TextNormal, TextNormal.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 + 105)))
        sc.blit(TextHard, TextHard.get_rect(center=(WINDOWS_SIZE[0] // 2 + 460, WINDOWS_SIZE[1] // 2 + 105)))
        sc.blit(EasyImage, EasyImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 460, WINDOWS_SIZE[1] // 2 - 65)))
        sc.blit(NormalImage, NormalImage.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 65)))
        sc.blit(HardImage, HardImage.get_rect(center=(WINDOWS_SIZE[0] // 2 + 460, WINDOWS_SIZE[1] // 2 - 65)))
        sc.blit(SelectImage, SelectImageRect)
        sc.blit(scene_menu.EKeyImage, scene_menu.EKeyImageRect)
        sc.blit(scene_menu.TextOK, scene_menu.TextOKRect)
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        fill_group.draw(sc)
        pygame.display.update()
        clock.tick(FPS)
        fill_group.update()
