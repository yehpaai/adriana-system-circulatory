from random import randint

import config_script
import effect_system
import group_config
import take_screenshot
from ITEM import create_item
from SCENE import scene_config, scene_die, scene_novel, scene_menu
from LOADING import loading
from collision import collisionItem, collisionBullet
from config_script import *
from font_config import *
from SKILLTREE import SkillTree
from NOTFICATION import notfication_manager
from sfx_compilation import ENTER_PAUSE, SELECT_MENU, ENTER_SELECT, EXIT_PAUSE
import reset_data
import pygame

LEVEL: int = 0

TEXT_SET = ("", "", "", "", "", "")
TEXT_SET_RANK = ("", "", "", "", "")
TEXT_SET_TASK = ("", "", "")

TextKillCount = FontKILL.render(TEXT_SET[0] + str(scene_config.KILL_COUNT) + "/" + str(scene_config.MAX_KILL),
                                scene_config.AA_TEXT, (255, 255, 255))
TextKillCountRect = TextKillCount.get_rect(center=(WINDOWS_SIZE[0] // 2 - 490, WINDOWS_SIZE[0]//2+40))

ExpText = FontEXP.render(TEXT_SET[1] + str(scene_config.p.EXP), scene_config.AA_TEXT, (255, 255, 255))
ExpTextRect = ExpText.get_rect(center=(32, 345))

textAlpha: int = 150

HealthText = HealthFont.render(str(scene_config.p.Health), scene_config.AA_TEXT, (255, 255, 255))
HealthTextRect = HealthText.get_rect(center=(WINDOWS_SIZE[0] // 2, 697))

PlayerPosText = PosFont.render("X: " + str(scene_config.p.rect.x) + " Y: " + str(scene_config.p.rect.y),
                               scene_config.AA_TEXT, (255, 255, 255))
PlayerPosRect = PlayerPosText.get_rect(center=(WINDOWS_SIZE[0] // 2 - 500, 710))

ScoreText = ScoreFont.render(TEXT_SET[3] + str(scene_config.Score), scene_config.AA_TEXT, (255, 255, 0))
ScoreTextRect = ScoreText.get_rect(center=(58, WINDOWS_SIZE[0]//2-598))

AmmoText = AmmoFont.render(TEXT_SET[2] + str(scene_config.p.Ammo), scene_config.AA_TEXT, (255, 255, 255))
AmmoTextRect = AmmoText.get_rect(center=(WINDOWS_SIZE[0] // 2 + 270, WINDOWS_SIZE[0]//2-600))

CrystalCount = FontKILL.render(TEXT_SET[5] + str(scene_config.CRYSTAL_COUNT) + "/" + str(scene_config.CRYSTAL_MAX),
                               scene_config.AA_TEXT, (255, 255, 255))
CrystalCountRect = CrystalCount.get_rect(center=(WINDOWS_SIZE[0] // 2 - 490, WINDOWS_SIZE[0]//2+40))

match config_script.LANGUAGE_SET:
    case "UA":
        TEXT_SET = (config_script.TEXT_LEVEL_UA[0], config_script.TEXT_LEVEL_UA[1], config_script.TEXT_LEVEL_UA[2],
                    config_script.TEXT_LEVEL_UA[3], config_script.TEXT_LEVEL_UA[4], config_script.TEXT_LEVEL_UA[5])
        TEXT_SET_RANK = (
            config_script.TEXT_RANK_UA[0], config_script.TEXT_RANK_UA[1], config_script.TEXT_RANK_UA[2],
            config_script.TEXT_RANK_UA[3], config_script.TEXT_RANK_UA[4])
        TEXT_SET_TASK = (config_script.TEXT_TASK_UA[0], config_script.TEXT_TASK_UA[1], config_script.TEXT_TASK_UA[2])
    case "EN":
        TEXT_SET = (config_script.TEXT_LEVEL_EN[0], config_script.TEXT_LEVEL_EN[1], config_script.TEXT_LEVEL_EN[2],
                    config_script.TEXT_LEVEL_EN[3], config_script.TEXT_LEVEL_EN[4], config_script.TEXT_LEVEL_EN[5])
        TEXT_SET_RANK = (
            config_script.TEXT_RANK_EN[0], config_script.TEXT_RANK_EN[1], config_script.TEXT_RANK_EN[2],
            config_script.TEXT_RANK_EN[3], config_script.TEXT_RANK_EN[4])
        TEXT_SET_TASK = (config_script.TEXT_TASK_EN[0], config_script.TEXT_TASK_EN[1], config_script.TEXT_TASK_EN[2])


def textUpdate():
    global TextKillCount, ExpText, textAlpha, HealthText, PlayerPosText, AmmoText, ScoreText, TEXT_SET, TEXT_SET_RANK, \
        AmmoTextRect, CrystalCount, ExpTextRect, HealthTextRect

    CrystalCount = FontKILL.render(TEXT_SET[5] + str(scene_config.CRYSTAL_COUNT) + "/" + str(scene_config.CRYSTAL_MAX),
                                   scene_config.AA_TEXT, (255, 255, 255))
    TextKillCount = FontKILL.render(TEXT_SET[0] + str(scene_config.KILL_COUNT) + "/" + str(scene_config.MAX_KILL),
                                    scene_config.AA_TEXT, (255, 255, 255))

    if scene_config.p.EXP < 500:
        ExpText = FontEXP.render(TEXT_SET[1] + str(scene_config.p.EXP), scene_config.AA_TEXT, (255, 255, 255))
    else:
        ExpText = FontEXP.render(TEXT_SET[1] + "MAX!", scene_config.AA_TEXT, (255, 255, 255))
    ExpTextRect = ExpText.get_rect(center=(WINDOWS_SIZE[0] // 2 - 270, WINDOWS_SIZE[0]//2-600))
    AmmoText = AmmoFont.render(TEXT_SET[2] + str(scene_config.p.Ammo), scene_config.AA_TEXT, (255, 255, 255))
    ScoreText = ScoreFont.render(TEXT_SET[3] + str(scene_config.Score), scene_config.AA_TEXT, (255, 255, 0))
    ScoreText.set_alpha(150)
    if textAlpha >= 50:
        textAlpha -= 2
    else:
        textAlpha = 150
    CrystalCount.set_alpha(textAlpha)
    TextKillCount.set_alpha(textAlpha)
    HealthText = HealthFont.render(str(scene_config.p.Health), scene_config.AA_TEXT, (255, 255, 255))
    HealthTextRect = HealthText.get_rect(center=(WINDOWS_SIZE[0] // 2, 697))
    PlayerPosText = PosFont.render("X: " + str(scene_config.p.rect.x) + " Y: " + str(scene_config.p.rect.y),
                                   scene_config.AA_TEXT, (255, 255, 255))


heartUI = createImage("ASSETS/SPRITE/UI/ICON/HeartUI.png")
heartUI = pygame.transform.scale(heartUI, (55, 54))
heartUIRect = heartUI.get_rect(center=(WINDOWS_SIZE[0] // 2, 696))


def updateUI():
    global heartUI
    heartUI.set_alpha(scene_config.p.Health)


timeCreateEffect: int = 0
createEffect: int = 0


def createEffectLevel():
    global timeCreateEffect, createEffect
    if createEffect < scene_config.TimeCountLevelEffect:
        match timeCreateEffect:
            case 5:
                createEffect += 1
                timeCreateEffect = 0
                match scene_config.isMoveBackEffect:
                    case True:
                        return effect_system.effectLevel(randint(-5000, 5000))
                    case False:
                        return effect_system.effectLevel(randint(5, 740))
            case _:
                timeCreateEffect += 1


def scene0_draw():
    group_config.back_effect_group.draw(sc)
    group_config.effect_group.draw(sc)
    for t in group_config.TextEffect_group:
        sc.blit(t.text, t.textRect)

    if scene_config.p.EXP >= 70:
        sc.blit(scene_config.PlayerProgresInOne, scene_config.PlayerProgresInOneRect)
    if scene_config.p.EXP >= 150:
        sc.blit(scene_config.PlayerProgresInTWO, scene_config.PlayerProgresInTWORect)
    if scene_config.isDrawPlayer:
        sc.blit(scene_config.p.image, scene_config.p.rect)
    group_config.bodies_effect_group.draw(sc)
    group_config.player_bullet.draw(sc)
    group_config.item_group.draw(sc)
    group_config.ball_rosa_group.draw(sc)
    group_config.shota_group.draw(sc)
    group_config.shutle_group.draw(sc)
    group_config.ball_soul.draw(sc)
    group_config.magical_girl_group.draw(sc)
    group_config.simple_bullet.draw(sc)
    group_config.pistol_bullet.draw(sc)
    group_config.anyBullet.draw(sc)
    group_config.notfication_group.draw(sc)
    for n in group_config.notfication_group:
        sc.blit(n.text, n.rectTxt)


def scene0_update():
    if scene_config.isDrawPlayer:
        scene_config.p.update()
    group_config.spin_group.update()
    group_config.ad_group.update()
    group_config.bodies_effect_group.update()
    group_config.anyBullet.update()
    group_config.fill_group.update()
    group_config.pistol_bullet.update()
    group_config.back_effect_group.update()
    group_config.effect_group.update()
    group_config.TextEffect_group.update()
    group_config.player_bullet.update()
    group_config.item_group.update()
    group_config.simple_bullet.update()
    group_config.notfication_group.update()
    textUpdate()
    updateUI()
    scene_config.playerPosSet()
    scene_config.createAd()


def scene0():
    global LEVEL

    create_item.create_exp(randint(100, 1120), randint(150, 495), 0)
    create_item.create_exp(randint(100, 1120), randint(150, 495), 1)
    create_item.create_ammo(randint(90, 1200), randint(150, 495))
    timeLevelSpawn: int = 0

    isTimeOver: bool = True
    TimeLeft: int = 30
    TimePerSecond: int = 0

    match scene_config.LEVEL_TASK:
        case 0:
            match LEVEL:
                case 1:
                    match scene_config.DIFFICULTY:
                        case 0:
                            scene_config.MAX_KILL = 15
                            TimeLeft = 35
                        case 1:
                            scene_config.MAX_KILL = 35
                            TimeLeft = 40
                        case 2:
                            scene_config.MAX_KILL = 45
                            TimeLeft = 55
                case 2:
                    match scene_config.DIFFICULTY:
                        case 0:
                            scene_config.MAX_KILL = 25
                            TimeLeft = 40
                        case 1:
                            scene_config.MAX_KILL = 65
                            TimeLeft = 60
                        case 2:
                            scene_config.MAX_KILL = 75
                            TimeLeft = 70

                case 3:
                    match scene_config.DIFFICULTY:
                        case 0:
                            scene_config.MAX_KILL = 35
                            TimeLeft = 50
                        case 1:
                            scene_config.MAX_KILL = 71
                            TimeLeft = 73
                        case 2:
                            scene_config.MAX_KILL = 85
                            TimeLeft = 81
                case 4:
                    match scene_config.DIFFICULTY:
                        case 0:
                            scene_config.MAX_KILL = 45
                            TimeLeft = 65
                        case 1:
                            scene_config.MAX_KILL = 86
                            TimeLeft = 80
                        case 2:
                            scene_config.MAX_KILL = 105
                            TimeLeft = 85
                case 5:
                    match scene_config.DIFFICULTY:
                        case 0:
                            scene_config.MAX_KILL = 50
                            TimeLeft = 70
                        case 1:
                            scene_config.MAX_KILL = 93
                            TimeLeft = 94
                        case 2:
                            scene_config.MAX_KILL = 135
                            TimeLeft = 100
                case 5:
                    match scene_config.DIFFICULTY:
                        case 0:
                            scene_config.MAX_KILL = 60
                            TimeLeft = 75
                        case 1:
                            scene_config.MAX_KILL = 110
                            TimeLeft = 111
                        case 2:
                            scene_config.MAX_KILL = 165
                            TimeLeft = 135
                case 6:
                    match scene_config.DIFFICULTY:
                        case 0:
                            scene_config.MAX_KILL = 70
                            TimeLeft = 85
                        case 1:
                            scene_config.MAX_KILL = 130
                            TimeLeft = 131
                        case 2:
                            scene_config.MAX_KILL = 195
                            TimeLeft = 165
                case 7:
                    match scene_config.DIFFICULTY:
                        case 0:
                            scene_config.MAX_KILL = 85
                            TimeLeft = 105
                        case 1:
                            scene_config.MAX_KILL = 155
                            TimeLeft = 145
                        case 2:
                            scene_config.MAX_KILL = 215
                            TimeLeft = 195
                case 8:
                    match scene_config.DIFFICULTY:
                        case 0:
                            scene_config.MAX_KILL = 90
                            TimeLeft = 125
                        case 1:
                            scene_config.MAX_KILL = 175
                            TimeLeft = 165
                        case 2:
                            scene_config.MAX_KILL = 265
                            TimeLeft = 235
                case 9:
                    match scene_config.DIFFICULTY:
                        case 0:
                            scene_config.MAX_KILL = 100
                            TimeLeft = 130
                        case 1:
                            scene_config.MAX_KILL = 195
                            TimeLeft = 175
                        case 2:
                            scene_config.MAX_KILL = 295
                            TimeLeft = 255
        case 1:
            match LEVEL:
                case 1:
                    TimeLeft = 35
                case 2:
                    TimeLeft = 50
                case 3:
                    TimeLeft = 65
                case 4:
                    TimeLeft = 70
                case 5:
                    TimeLeft = 90
                case 6:
                    TimeLeft = 100
                case 7:
                    TimeLeft = 115
                case 8:
                    TimeLeft = 120
                case 9:
                    TimeLeft = 135
        case 2:
            create_item.create_crystal_task()
            match LEVEL:
                case 0:
                    TimeLeft = 25
                    scene_config.CRYSTAL_MAX = 5
                case 1:
                    TimeLeft = 30
                    scene_config.CRYSTAL_MAX = 10
                case 2:
                    TimeLeft = 35
                    scene_config.CRYSTAL_MAX = 15
                case 3:
                    TimeLeft = 45
                    scene_config.CRYSTAL_MAX = 25
                case 4:
                    TimeLeft = 52
                    scene_config.CRYSTAL_MAX = 30
                case 5:
                    TimeLeft = 57
                    scene_config.CRYSTAL_MAX = 35
                case 6:
                    TimeLeft = 61
                    scene_config.CRYSTAL_MAX = 45
                case 7:
                    TimeLeft = 75
                    scene_config.CRYSTAL_MAX = 55
                case 8:
                    TimeLeft = 85
                    scene_config.CRYSTAL_MAX = 65
                case 9:
                    TimeLeft = 95
                    scene_config.CRYSTAL_MAX = 75

    ClockIcon = createImage("ASSETS/SPRITE/ITEM/clock.png")
    ClockIcon = pygame.transform.scale(ClockIcon, (32, 31))
    ClockIcon.set_alpha(115)
    ClockRect = ClockIcon.get_rect(center=(WINDOWS_SIZE[0] // 2 - 22, 38))

    ColorTime = [255, 255, 255]
    TextTime = FontTime.render(str(TimeLeft), scene_config.AA_TEXT, ColorTime)
    TextTime.set_alpha(120)
    TextTimeRect = TextTime.get_rect(center=(WINDOWS_SIZE[0] // 2 + 9, 40))

    sizeAdriana: int = 1795
    ForceSizeAdriana: int = 0

    AdrianaIcon = createImage("ASSETS/SPRITE/UI/ICON/AdrianaMax/1.png")
    AdrianaIcon = pygame.transform.scale(AdrianaIcon, (sizeAdriana, sizeAdriana))
    AdrianaIconRect = AdrianaIcon.get_rect(center=(WINDOWS_SIZE[0] // 2, 200))

    isFinish: bool = False
    timeFinishEnd: int = 0
    FrameAdriana: int = 0

    TimeAnimAdriana: int = 0

    FinishText = FinishFont.render(TEXT_SET[4], scene_config.AA_TEXT, (255, 255, 0))
    FinishTextRect = FinishText.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 20))
    AlphaTextFinish: int = 0
    FinishText.set_alpha(AlphaTextFinish)
    timeWaitText: int = 0

    BlackFill = pygame.image.load("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png").convert()
    BlackFill = pygame.transform.scale(BlackFill, WINDOWS_SIZE)
    BlackFillRect = BlackFill.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))
    FrameFillBlack: int = 0
    BlackFill.set_alpha(FrameFillBlack)

    LevelRang: str = "-"

    isRank: bool = False
    ColorRank = (255, 255, 255)
    RankTextLevel = RankFont.render(LevelRang, scene_config.AA_TEXT, ColorRank)
    RankRectLevel = RankTextLevel.get_rect(center=(1165, 100))

    isUpdate: bool = True
    running: bool = True

    FPS: int = 60

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")

    fpsText = fpsFont.render(str(int(clock.get_fps())), False, (255, 255, 255))
    fpsRect = fpsText.get_rect(center=(43, 680))

    TaskText = FontRank.render(TEXT_SET_TASK[scene_config.LEVEL_TASK], scene_config.AA_TEXT, (255, 255, 255))
    TaskTextRect = TaskText.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, config_script.WINDOWS_SIZE[1] // 2))
    isAnimTask: bool = True
    FrameTask: int = 0
    AlphaTextTask: int = 0
    TaskText.set_alpha(AlphaTextTask)

    timeDie: int = 0

    iconPause = ("ASSETS/SPRITE/UI/PAUSE MENU/ICON/1.png", "ASSETS/SPRITE/UI/PAUSE MENU/ICON/2.png",
                 "ASSETS/SPRITE/UI/PAUSE MENU/ICON/3.png", "ASSETS/SPRITE/UI/PAUSE MENU/ICON/4.png")

    PauseIcon = config_script.createImage(iconPause[0])
    PauseIcon = pygame.transform.scale(PauseIcon, (129, 129))
    AnglePauseIcon: int = 0
    selectIcon: int = 0

    FillImagePause = pygame.image.load("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png").convert()
    FillImagePause = pygame.transform.scale(FillImagePause, scene_config.WINDOWS_SIZE)
    FillImagePauseRect = FillImagePause.get_rect(
        center=(scene_config.WINDOWS_SIZE[0] // 2, scene_config.WINDOWS_SIZE[1] // 2))
    FillPauseAlpha: int = 0
    FillImagePause.set_alpha(FillPauseAlpha)

    TextPause = ("", "", "", "")

    match config_script.LANGUAGE_SET:
        case "UA":
            TextPause = ("Вертайся Пупсік", "Почати З Початку", "Вийти", "УВАГА, ПАУЗА НА СЦЕНІ!")
        case "EN":
            TextPause = ("Back To Game", "Restart Level", "Exit", "WARNING, PAUSE IN THE SCENE!")

    BackToGameText = TextOption.render(TextPause[0], scene_config.AA_TEXT, (255, 255, 255))
    BackToGameTextRect = BackToGameText.get_rect(center=(185, 295))

    RestartGameText = TextOption.render(TextPause[1], scene_config.AA_TEXT, (255, 255, 255))
    RestartGameTextRect = RestartGameText.get_rect(center=(186, 365))

    ExitGameText = TextOption.render(TextPause[2], scene_config.AA_TEXT, (255, 255, 255))
    ExitGameTextRect = ExitGameText.get_rect(center=(186, 445))

    WarningTextPause = FinishFont.render(TextPause[3], scene_config.AA_TEXT, (255, 255, 0))
    WarningTextPauseRect = WarningTextPause.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, 85))

    ArrowImage = config_script.createImage("ASSETS/SPRITE/UI/BUTTON/ARROW/1.png")
    ArrowImage = pygame.transform.scale(ArrowImage, (34, 34))
    ArrowImageRect = ArrowImage.get_rect(center=(55, 295))

    TEXT_SET_RANK_FONT = "Ранг:"

    match config_script.LANGUAGE_SET:
        case "EN":
            TEXT_SET_RANK_FONT = "RANK:"

    RankText = ScoreFont.render(TEXT_SET_RANK_FONT, scene_config.AA_TEXT, (255, 255, 255))
    RankTextRect = RankText.get_rect(center=(1165, 70))

    selectPause: int = 0

    FillAd = pygame.image.load("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png").convert()
    FillAd = pygame.transform.scale(FillAd, config_script.WINDOWS_SIZE)
    FillAdRect = FillAd.get_rect(center=(config_script.WINDOWS_SIZE[0] // 2, config_script.WINDOWS_SIZE[1] // 2))

    FillAdAlpha: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.select_scene = None
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take_screenshot.take()
                if e.key == pygame.K_ESCAPE:
                    match isUpdate:
                        case False:
                            EXIT_PAUSE.play()
                            selectIcon = randint(0, 3)
                            isUpdate = True
                        case True:
                            ENTER_PAUSE.play()
                            FillPauseAlpha = 0
                            isUpdate = False
                if e.key == pygame.K_DOWN and not isUpdate:
                    SELECT_MENU.play()
                    match selectPause:
                        case 0:
                            selectPause = 1
                        case 1:
                            selectPause = 2
                        case 2:
                            selectPause = 0
                if e.key == pygame.K_UP and not isUpdate:
                    SELECT_MENU.play()
                    match selectPause:
                        case 0:
                            selectPause = 2
                        case 1:
                            selectPause = 0
                        case 2:
                            selectPause = 1
                if e.key == pygame.K_e and not isUpdate:
                    ENTER_SELECT.play()
                    match selectPause:
                        case 0:
                            isUpdate = True
                        case 1:
                            reset_data.reset()
                            running = False
                            scene_config.switch_scene(loading.loading)
                        case 2:
                            reset_data.reset()
                            running = False
                            scene_config.switch_scene(scene_menu.scene_menu)

        # draw
        sc.fill((8, 0, 24))
        scene0_draw()
        match scene_config.LEVEL_TASK:
            case 0:
                sc.blit(TextKillCount, TextKillCountRect)
            case 2:
                sc.blit(CrystalCount, CrystalCountRect)
        sc.blit(ExpText, ExpTextRect)
        pygame.draw.rect(sc, (0, 0, 0), (0, 0, WINDOWS_SIZE[0], WINDOWS_SIZE[1]), 19)
        pygame.draw.rect(sc, (255, 255, 255), (19, 15, 1245, 685), 1)
        if scene_config.isShowPos:
            sc.blit(scene_config.PlayerPosImage, scene_config.PlayerPosRect)
        sc.blit(heartUI, heartUIRect)
        sc.blit(HealthText, HealthTextRect)
        sc.blit(PlayerPosText, PlayerPosRect)
        sc.blit(AmmoText, AmmoTextRect)
        sc.blit(ScoreText, ScoreTextRect)
        sc.blit(ClockIcon, ClockRect)
        sc.blit(TextTime, TextTimeRect)
        group_config.spin_group.draw(sc)
        match scene_config.FPS_SHOW:
            case True:
                fpsText = fpsFont.render(str(int(clock.get_fps())), False, (255, 255, 255))
                sc.blit(fpsText, fpsRect)
        match isUpdate:
            case True:
                match isRank:
                    case True:
                        sc.blit(RankText, RankTextRect)
                        sc.blit(RankTextLevel, RankRectLevel)

                if scene_config.p.Health <= 0:
                    scene_config.p.Health = 0
                    scene_config.isDrawPlayer = False
                    if timeDie >= 315:
                        scene_config.switch_scene(scene_die.scene_die)
                        running = False
                    if timeDie >= 85:
                        FrameFillBlack += 2
                        BlackFill.set_alpha(FrameFillBlack)
                        sc.blit(BlackFill, BlackFillRect)
                    timeDie += 1

                match scene_config.LEVEL_TASK:
                    case 0:
                        if scene_config.KILL_COUNT >= scene_config.MAX_KILL:
                            isFinish = True
                        match isTimeOver:
                            case True:
                                if TimeLeft < -1:
                                    scene_config.switch_scene(scene_die.scene_time_over)
                                    running = False
                                match TimePerSecond:
                                    case 250:
                                        TimePerSecond = 100
                                        match TimeLeft:
                                            case 15:
                                                ColorTime = [255, 0, 0]
                                        TextTime = FontTime.render(str(TimeLeft), scene_config.AA_TEXT, ColorTime)
                                        TimeLeft -= 1
                                    case _:
                                        TimePerSecond += 1
                    case 1:
                        match isTimeOver:
                            case True:
                                if TimeLeft < 0:
                                    isTimeOver = False
                                    isFinish = True
                                match TimePerSecond:
                                    case 200:
                                        TimePerSecond = 100
                                        match TimeLeft:
                                            case 15:
                                                ColorTime = [0, 255, 0]
                                        TextTime = FontTime.render(str(TimeLeft), scene_config.AA_TEXT, ColorTime)
                                        TimeLeft -= 1
                                    case _:
                                        TimePerSecond += 1
                    case 2:
                        if scene_config.CRYSTAL_COUNT >= scene_config.CRYSTAL_MAX:
                            isFinish = True
                        match isTimeOver:
                            case True:
                                if TimeLeft < -1:
                                    scene_config.switch_scene(scene_die.scene_time_over)
                                    running = False
                                match TimePerSecond:
                                    case 250:
                                        TimePerSecond = 100
                                        match TimeLeft:
                                            case 15:
                                                ColorTime = [255, 0, 0]
                                        TextTime = FontTime.render(str(TimeLeft), scene_config.AA_TEXT, ColorTime)
                                        TimeLeft -= 1
                                    case _:
                                        TimePerSecond += 1

                if scene_config.KILL_COUNT >= 5:
                    LevelRang = 'E'
                    scene_config.p.MultipleScore = 0
                    ColorRank = (155, 0, 0)
                if scene_config.KILL_COUNT >= 25 and scene_config.Score >= 950:
                    LevelRang = 'D'
                    scene_config.p.MultipleScore = 5
                    ColorRank = (255, 0, 0)
                if scene_config.KILL_COUNT >= 35 and scene_config.Score >= 1250 or scene_config.p.Health >= 100:
                    LevelRang = 'C'
                    scene_config.p.MultipleScore = 10
                    ColorRank = (250, 155, 0)
                if scene_config.KILL_COUNT >= 45 and scene_config.Score >= 1750 or scene_config.p.Health >= 160:
                    LevelRang = 'B'
                    scene_config.p.MultipleScore = 20
                    ColorRank = (0, 19, 255)
                if scene_config.KILL_COUNT >= 85 and scene_config.Score >= 2350 or scene_config.p.Health >= 225:
                    LevelRang = 'A'
                    scene_config.p.MultipleScore = 30
                    ColorRank = (0, 255, 0)
                if scene_config.KILL_COUNT >= 135 and scene_config.Score >= 9350 and scene_config.p.Health >= 245:
                    LevelRang = 'S'
                    scene_config.p.MultipleScore = 40
                    ColorRank = (255, 255, 0)

                if scene_config.isFillAd:
                    FillAdAlpha += 0.5
                    FillAd.set_alpha(FillAdAlpha)
                    sc.blit(FillAd, FillAdRect)
                else:
                    FillAdAlpha = 0

                group_config.ad_group.draw(sc)

                match isFinish:
                    case True:
                        isTimeOver = False
                        if timeFinishEnd <= 330:
                            match TimeAnimAdriana:
                                case 195:
                                    timeFinishEnd += 1
                                    match FrameAdriana:
                                        case 20:
                                            AdrianaIcon = createImage("ASSETS/SPRITE/UI/ICON/AdrianaMax/1.png")
                                            AdrianaIcon = pygame.transform.scale(AdrianaIcon,
                                                                                 (sizeAdriana, sizeAdriana))
                                        case 35:
                                            AdrianaIcon = createImage("ASSETS/SPRITE/UI/ICON/AdrianaMax/2.png")
                                            AdrianaIcon = pygame.transform.scale(AdrianaIcon,
                                                                                 (sizeAdriana, sizeAdriana))
                                            FrameAdriana = 0
                                    FrameAdriana += 1
                                    if sizeAdriana > 280:
                                        sizeAdriana -= ForceSizeAdriana
                                        ForceSizeAdriana += 5
                                        AdrianaIcon = pygame.transform.scale(AdrianaIcon, (sizeAdriana, sizeAdriana))
                                        AdrianaIconRect = AdrianaIcon.get_rect(center=(WINDOWS_SIZE[0] // 2, 200))
                                    else:
                                        match timeWaitText:
                                            case 35:
                                                sc.blit(FinishText, FinishTextRect)
                                                AlphaTextFinish += 6
                                                FinishText.set_alpha(AlphaTextFinish)
                                            case _:
                                                timeWaitText += 1
                                    sc.blit(AdrianaIcon, AdrianaIconRect)
                                case _:
                                    TimeAnimAdriana += 1
                        else:
                            if FrameFillBlack <= 315:
                                FrameFillBlack += 5
                                BlackFill.set_alpha(FrameFillBlack)
                            else:
                                LEVEL += 1
                                running = False
                                scene_config.switch_scene(scene1)
                            sc.blit(BlackFill, BlackFillRect)

                group_config.fill_group.draw(sc)

                # update
                if isAnimTask:
                    if FrameTask <= 180:
                        FrameTask += 1
                        AlphaTextTask += 5
                        TaskText.set_alpha(AlphaTextTask)
                    else:
                        if AlphaTextTask >= 1:
                            AlphaTextTask -= 5
                            TaskText.set_alpha(AlphaTextTask)
                        else:
                            isAnimTask = False
                    sc.blit(TaskText, TaskTextRect)
                if scene_config.isDrawPlayer:
                    collisionItem()
                    collisionBullet()
                scene0_update()
                RankTextLevel = RankFont.render(LevelRang, scene_config.AA_TEXT, ColorRank)
                match timeLevelSpawn:
                    case 500:
                        match scene_config.LEVEL_TASK:
                            case 2:
                                scene_config.createCrystal()
                        group_config.shota_group.update()
                        group_config.ball_rosa_group.update()
                        group_config.shutle_group.update()
                        group_config.magical_girl_group.update()
                        group_config.ball_soul.update()
                        isRank = True
                    case _:
                        timeLevelSpawn += 1
            case False:
                match selectPause:
                    case 0:
                        ArrowImageRect = ArrowImage.get_rect(center=(55, BackToGameTextRect.y + 15))
                    case 1:
                        ArrowImageRect = ArrowImage.get_rect(center=(50, RestartGameTextRect.y + 15))
                    case 2:
                        ArrowImageRect = ArrowImage.get_rect(center=(110, ExitGameTextRect.y + 15))
                if FillPauseAlpha <= 165:
                    FillPauseAlpha += 11
                    FillImagePause.set_alpha(FillPauseAlpha)
                PauseIcon = config_script.createImage(iconPause[selectIcon])
                PauseIcon = pygame.transform.scale(PauseIcon, (129, 129))
                AnglePauseIcon += 1
                PauseIcon = pygame.transform.rotate(PauseIcon, AnglePauseIcon)
                PauseIconRect = PauseIcon.get_rect(center=(980, config_script.WINDOWS_SIZE[1] // 2))
                sc.blit(FillImagePause, FillImagePauseRect)
                sc.blit(PauseIcon, PauseIconRect)
                sc.blit(BackToGameText, BackToGameTextRect)
                sc.blit(RestartGameText, RestartGameTextRect)
                sc.blit(ExitGameText, ExitGameTextRect)
                sc.blit(ArrowImage, ArrowImageRect)
                sc.blit(WarningTextPause, WarningTextPauseRect)
        pygame.display.update()
        clock.tick(FPS)


def scene1():
    global LEVEL, createEffect

    FPS: int = 60
    running: bool = True

    BlackFill = createImage("ASSETS/SPRITE/UI/BACKGROUND/COLOR/black.png")
    BlackFill = pygame.transform.scale(BlackFill, WINDOWS_SIZE)
    BlackFillRect = BlackFill.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2))
    AlphaBlack: int = 550
    BlackFill.set_alpha(AlphaBlack)

    for ball_rosa in group_config.ball_rosa_group:
        ball_rosa.kill()
    for shota in group_config.shota_group:
        shota.kill()
    for shutle in group_config.shutle_group:
        shutle.kill()
    for soulBall in group_config.ball_soul:
        soulBall.kill()
    for magical in group_config.magical_girl_group:
        magical.kill()
    for bullet_sim in group_config.simple_bullet:
        bullet_sim.kill()
    for p_b in group_config.player_bullet:
        p_b.kill()
    for ab in group_config.anyBullet:
        ab.kill()
    for pistol in group_config.pistol_bullet:
        pistol.kill()
    for crystal in group_config.item_group:
        crystal.kill()
    for eg in group_config.effect_group:
        eg.kill()

    isKey: bool = False

    isAnimRank: bool = True

    sizeRank: int = 990
    ForceSizeRank: int = 1

    fileRank = "ASSETS/SPRITE/UI/RANK/2.png"

    if scene_config.KILL_COUNT >= 15 or scene_config.Score >= 250 or scene_config.p.Health >= 90:
        fileRank = "ASSETS/SPRITE/UI/RANK/1.png"
    if scene_config.KILL_COUNT >= 35 or scene_config.Score >= 450 or scene_config.p.Health >= 120:
        fileRank = "ASSETS/SPRITE/UI/RANK/3.png"
    if scene_config.KILL_COUNT >= 65 or scene_config.Score >= 750 or scene_config.p.Health >= 190:
        fileRank = "ASSETS/SPRITE/UI/RANK/4.png"
    if scene_config.KILL_COUNT >= 95 or scene_config.Score >= 450 or scene_config.p.Health >= 250:
        fileRank = "ASSETS/SPRITE/UI/RANK/5.png"
    if scene_config.KILL_COUNT >= 195 and scene_config.Score >= 1150 or scene_config.p.Health >= 315:
        fileRank = "ASSETS/SPRITE/UI/RANK/6.png"

    rankImage = createImage(fileRank)
    rankImage = pygame.transform.scale(rankImage, (sizeRank, sizeRank))
    rankRect = rankImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 30, WINDOWS_SIZE[0] // 2))

    TextRank = FontRank.render(TEXT_SET_RANK[0], scene_config.AA_TEXT, (255, 255, 0))
    TextRankRect = TextRank.get_rect(center=(WINDOWS_SIZE[0] // 2 - 20, 135))

    isAnimStat: bool = False
    statAlpha: int = 0

    KillCountText = FontStat.render(TEXT_SET_RANK[1] + str(scene_config.KILL_COUNT), scene_config.AA_TEXT,
                                    (255, 255, 255))
    KillCountRect = KillCountText.get_rect(center=(990, 230))

    HealthStatText = FontStat.render(TEXT_SET_RANK[2] + str(scene_config.p.Health), scene_config.AA_TEXT,
                                     (255, 255, 255))
    HealthStatRect = HealthStatText.get_rect(center=(990, 330))

    ScoreStatText = FontStat.render(TEXT_SET_RANK[3] + str(scene_config.Score), scene_config.AA_TEXT,
                                    (255, 255, 255))
    ScoreStatRect = ScoreStatText.get_rect(center=(990, 450))

    ScoreStatText.set_alpha(statAlpha)
    HealthStatText.set_alpha(statAlpha)
    KillCountText.set_alpha(statAlpha)

    ButtonAlpha: int = 0
    ButtonText = FontStat.render(TEXT_SET_RANK[4], scene_config.AA_TEXT, (255, 255, 255))
    ButtonTextRect = ButtonText.get_rect(center=(WINDOWS_SIZE[0] // 2, 590))

    ButtonText.set_alpha(ButtonAlpha)

    AlphaBlack_Start: int = 0
    isAnimBlack: bool = False

    TEXT_SET_NOT = ("", "")

    match LANGUAGE_SET:
        case "UA":
            TEXT_SET_NOT = (TEXT_NOTFICATION_UA[0], TEXT_NOTFICATION_UA[1])
        case "EN":
            TEXT_SET_NOT = (TEXT_NOTFICATION_EN[0], TEXT_NOTFICATION_EN[1])

    notfication_manager.notfication_create("ASSETS/SPRITE/UI/Notication/1.png", TEXT_SET_NOT[0])

    scene_config.p.Ammo += randint(150, 250)
    scene_config.p.Health += 35
    scene_config.KILL_COUNT = 0
    scene_config.CRYSTAL_COUNT = 0
    scene_config.isDrawPlayer = True
    if scene_config.p.EXP >= 45:
        scene_config.p.EXP -= 25
    scene_config.LEVEL_TASK = randint(0, 2)
    scene_config.p.rect.x = WINDOWS_SIZE[0] // 2 + 5
    scene_config.p.rect.y = WINDOWS_SIZE[0] // 2 + 5

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            elif e.type == pygame.KEYDOWN and isKey:
                if e.key == pygame.K_F6:
                    take_screenshot.take()
                if e.key != pygame.K_y and e.key != pygame.K_F6:
                    isAnimBlack = True
                else:
                    scene_config.switch_scene(SkillTree.skill_tree)
                    running = False

        sc.fill((24, 19, 0))
        group_config.back_effect_group.draw(sc)
        if AlphaBlack > -255:
            AlphaBlack -= 6
            BlackFill.set_alpha(AlphaBlack)
            sc.blit(BlackFill, BlackFillRect)
        else:
            sc.blit(TextRank, TextRankRect)
            match isAnimRank:
                case True:
                    if sizeRank > 230:
                        sizeRank -= ForceSizeRank
                        ForceSizeRank += 6
                        rankImage = pygame.image.load(fileRank).convert_alpha()
                        rankImage = pygame.transform.scale(rankImage, (sizeRank, sizeRank))
                        rankRect = rankImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 30, WINDOWS_SIZE[0] // 2 - 310))
                    else:
                        isAnimStat = True
                        isAnimRank = False
            match isAnimStat:
                case True:
                    if statAlpha <= 250:
                        statAlpha += 5
                        ScoreStatText.set_alpha(statAlpha)
                        HealthStatText.set_alpha(statAlpha)
                        KillCountText.set_alpha(statAlpha)
                    else:
                        if ButtonAlpha <= 250:
                            ButtonAlpha += 5
                            ButtonText.set_alpha(ButtonAlpha)
                        else:
                            isAnimStat = False
                        isKey = True
            sc.blit(rankImage, rankRect)
            sc.blit(KillCountText, KillCountRect)
            sc.blit(ScoreStatText, ScoreStatRect)
            sc.blit(HealthStatText, HealthStatRect)
            match isKey:
                case True:
                    sc.blit(ButtonText, ButtonTextRect)
                    sc.blit(BlackFill, BlackFillRect)
                    match isAnimBlack:
                        case True:
                            if AlphaBlack_Start < 330:
                                AlphaBlack_Start += 5
                                BlackFill.set_alpha(AlphaBlack_Start)
                            else:
                                match LEVEL:
                                    case 5:
                                        running = False
                                        scene_config.switch_scene(scene_novel.scene0)
                                    case 8:
                                        running = False
                                        scene_config.switch_scene(scene_novel.scene1)
                                    case _:
                                        for i in range(15):
                                            for b in group_config.back_effect_group:
                                                createEffect = 0
                                                b.kill()
                                        match scene_config.isLoading:
                                            case True:
                                                running = False
                                                scene_config.switch_scene(loading.loading)
                                            case False:
                                                running = False
                                                scene_config.switch_scene(scene0)

        group_config.notfication_group.draw(sc)
        for n in group_config.notfication_group:
            sc.blit(n.text, n.rectTxt)
        pygame.display.update()
        clock.tick(FPS)
        group_config.notfication_group.update()
        group_config.back_effect_group.update()
