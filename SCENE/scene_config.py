from random import randint

import pygame.transform

import ad_system
import effect_system
from PLAYER import player
from config_script import *
from ITEM import create_item

select_scene: None = None

p = player.Player(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2, 15, "ASSETS/SPRITE/PLAYER/QWERTY.png")
isDrawPlayer: bool = True

globalRank: str = '-'
Score: int = 0

isShowPos: bool = True

PlayerPosImage = pygame.image.load("ASSETS/SPRITE/UI/ICON/PlayerPos.png").convert_alpha()
PlayerPosImage = pygame.transform.scale(PlayerPosImage, (31, 43))
PlayerPosRect = PlayerPosImage.get_rect(center=(p.rect.x, 689))
PlayerPosImage.set_alpha(85)

PlayerProgresInOne = createImage("ASSETS/SPRITE/PLAYER/Indicator.png")
PlayerProgresInOne = pygame.transform.scale(PlayerProgresInOne, (14, 14))
PlayerProgresInOneRect = PlayerProgresInOne.get_rect(center=(p.rect.x + 15, p.rect.y + 11))
AngleOne: int = 0
AngleSpeed: int = 1

PlayerProgresInTWO = createImage("ASSETS/SPRITE/PLAYER/Indicator.png")
PlayerProgresInTWO = pygame.transform.scale(PlayerProgresInTWO, (14, 14))
PlayerProgresInTWORect = PlayerProgresInTWO.get_rect(center=(p.rect.x - 5, p.rect.y + 11))


def createFill(filename, force=10):
    return effect_system.colorFill(filename, force)


def playerPosSet():
    global PlayerPosRect, PlayerProgresInOneRect, PlayerProgresInOne, AngleOne, PlayerProgresInTWO, \
        PlayerProgresInTWORect
    PlayerPosRect = PlayerPosImage.get_rect(center=(p.rect.x + 6, 689))
    PlayerPosImage.set_alpha(p.rect.y - 35)

    PlayerProgresInOne = createImage("ASSETS/SPRITE/PLAYER/Indicator.png")
    PlayerProgresInOne = pygame.transform.scale(PlayerProgresInOne, (14, 14))
    AngleOne += AngleSpeed
    PlayerProgresInOne = pygame.transform.rotate(PlayerProgresInOne, AngleOne)
    PlayerProgresInOneRect = PlayerProgresInOne.get_rect(center=(p.rect.x + 27, p.rect.y + 11))

    PlayerProgresInTWO = createImage("ASSETS/SPRITE/PLAYER/Indicator.png")
    PlayerProgresInTWO = pygame.transform.scale(PlayerProgresInTWO, (14, 14))
    AngleOne += AngleSpeed
    PlayerProgresInTWO = pygame.transform.rotate(PlayerProgresInTWO, AngleOne)
    PlayerProgresInTWORect = PlayerProgresInTWO.get_rect(center=(p.rect.x - 9, p.rect.y + 11))


timeCreateAdMax: int = randint(365, 650)
timeCreateAd: int = 0

isCreateAD: bool = True


def createAd():
    global timeCreateAd
    if isCreateAD:
        if timeCreateAd > timeCreateAdMax:
            timeCreateAd = 0
            return ad_system.ad()
        else:
            timeCreateAd += 1


isMoveBackEffect: bool = True
TimeCountLevelEffect: int = 250

isParticle: bool = True
isParticleMore: bool = True
isRotateObject: bool = True
isLoading: bool = True

timeCreateCrystal: int = 0
timeCreateCrystalMax: int = randint(200, 300)


def createCrystal():
    global timeCreateCrystal
    if timeCreateCrystal >= timeCreateCrystalMax:
        create_item.create_crystal_task()
        timeCreateCrystal = 0
    else:
        timeCreateCrystal += 1


LEVEL_TASK: int = 2

isFillAd: bool = False

KILL_COUNT: int = 0
MAX_KILL: int = 0
CRYSTAL_COUNT: int = 0
CRYSTAL_MAX: int = 0

TRASH_COUNT: int = 15

isVisibleKey: bool = False

AA_TEXT: bool = False

FPS_SHOW: bool = True

DIFFICULTY: int = 0

GameMode: int = 0


def switch_scene(scene):
    global select_scene
    select_scene = scene
