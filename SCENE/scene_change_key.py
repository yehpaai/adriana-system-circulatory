import pygame
import key_setup
from SCENE import scene_config, scene_select_setings, scene_menu
from config_script import sc, clock, createImage, LANGUAGE_SET, WINDOWS_SIZE
from font_config import TextOption, FontKILL
from group_config import fill_group
from sfx_compilation import ENTER_SELECT, SELECT_MENU


def scene():
    running: bool = True
    FPS: int = 60

    TEXT_SET = (
        "Рухатись в верх: ", "Рухатись в Низ: ", "Рухатись в Ліво: ", "Рухатись в Право: ", "Стріляти: ",
        "Стріляти в Ліво: ",
        "Стріляти в Право: ")

    match LANGUAGE_SET:
        case "EN":
            TEXT_SET = (
                "Move Up: ", "Move Down: ", "Move Left: ", "Move Right: ", "Shoot: ", "Shoot Left: ",
                "Shoov Right: ")

    select: int = 0

    isSetKey: bool = False

    TextUp = TextOption.render(TEXT_SET[0] + str("w"), scene_config.AA_TEXT, (255, 255, 255))

    TextDown = TextOption.render(TEXT_SET[1] + str("s"), scene_config.AA_TEXT, (255, 255, 255))

    TextLeft = TextOption.render(TEXT_SET[2] + str("a"), scene_config.AA_TEXT, (255, 255, 255))

    TextRight = TextOption.render(TEXT_SET[3] + str("d"), scene_config.AA_TEXT, (255, 255, 255))

    arrowImage = pygame.image.load("ASSETS/SPRITE/UI/BUTTON/ARROW/1.png").convert_alpha()
    arrowImage = pygame.transform.scale(arrowImage, (32, 32))
    arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 45, WINDOWS_SIZE[1] // 2 - 150))

    TextQUIT = FontKILL.render(":QUIT", scene_config.AA_TEXT, (255, 255, 255))
    TextQUITRect = TextQUIT.get_rect(center=(1130, 680))

    QKeyImage = createImage("ASSETS/SPRITE/UI/KEY/Q.png")
    QKeyImage = pygame.transform.scale(QKeyImage, (31, 31))
    QKeyImageRect = QKeyImage.get_rect(center=(1082, 680))

    FrameText: int = 0
    FrameNotText: int = 25

    scene_config.createFill("ASSETS/SPRITE/UI/BACKGROUND/COLOR/white.png", 12)

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if not isSetKey:
                    if e.key == pygame.K_q:
                        running = False
                        scene_config.switch_scene(scene_select_setings.scene)
                    if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                        ENTER_SELECT.play()
                        isSetKey = True
                    if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                        SELECT_MENU.play()
                        match select:
                            case 0:
                                select = 1
                            case 1:
                                select = 2
                            case 2:
                                select = 3
                            case 3:
                                select = 0
                    if e.key == pygame.K_UP or e.key == pygame.K_w:
                        SELECT_MENU.play()
                        match select:
                            case 0:
                                select = 3
                            case 1:
                                select = 0
                            case 2:
                                select = 1
                            case 3:
                                select = 2
                else:
                    if e.key != pygame.K_CAPSLOCK and e.key != pygame.K_TAB and e.key != pygame.K_BACKSPACE:
                        match select:
                            case 0:
                                isSetKey = False
                                TextUp = TextOption.render(TEXT_SET[0] + str(e.unicode), scene_config.AA_TEXT,
                                                           (255, 255, 255))
                                key_setup.PLAYER_MOVE[0] = e.key
                            case 1:
                                isSetKey = False
                                TextDown = TextOption.render(TEXT_SET[1] + str(e.unicode), scene_config.AA_TEXT,
                                                             (255, 255, 255))
                                key_setup.PLAYER_MOVE[1] = e.key
                            case 2:
                                isSetKey = False
                                TextLeft = TextOption.render(TEXT_SET[2] + str(e.unicode), scene_config.AA_TEXT,
                                                             (255, 255, 255))
                                key_setup.PLAYER_MOVE[2] = e.key
                            case 3:
                                isSetKey = False
                                TextRight = TextOption.render(TEXT_SET[3] + str(e.unicode), scene_config.AA_TEXT,
                                                              (255, 255, 255))
                                key_setup.PLAYER_MOVE[3] = e.key

        if isSetKey:
            match select:
                case 0:
                    if FrameText < 25 and FrameNotText > 0:
                        FrameText += 1
                        FrameNotText -= 1
                        TextUp = TextOption.render(TEXT_SET[0] + "", scene_config.AA_TEXT, (255, 255, 255))
                    else:
                        if FrameText > 0:
                            TextUp = TextOption.render("   " + TEXT_SET[0] + "_", scene_config.AA_TEXT, (255, 255, 255))
                            FrameText -= 1
                        else:
                            FrameNotText = 25
                case 1:
                    if FrameText < 25 and FrameNotText > 0:
                        FrameText += 1
                        FrameNotText -= 1
                        TextDown = TextOption.render(TEXT_SET[1] + "", scene_config.AA_TEXT, (255, 255, 255))
                    else:
                        if FrameText > 0:
                            TextDown = TextOption.render("   " + TEXT_SET[1] + "_", scene_config.AA_TEXT,
                                                         (255, 255, 255))
                            FrameText -= 1
                        else:
                            FrameNotText = 25
                case 2:
                    if FrameText < 25 and FrameNotText > 0:
                        FrameText += 1
                        FrameNotText -= 1
                        TextLeft = TextOption.render(TEXT_SET[2] + "", scene_config.AA_TEXT, (255, 255, 255))
                    else:
                        if FrameText > 0:
                            TextLeft = TextOption.render("   " + TEXT_SET[2] + "_", scene_config.AA_TEXT,
                                                         (255, 255, 255))
                            FrameText -= 1
                        else:
                            FrameNotText = 25
                case 3:
                    if FrameText < 25 and FrameNotText > 0:
                        FrameText += 1
                        FrameNotText -= 1
                        TextRight = TextOption.render(TEXT_SET[3] + "", scene_config.AA_TEXT, (255, 255, 255))
                    else:
                        if FrameText > 0:
                            TextRight = TextOption.render("   " + TEXT_SET[3] + "_", scene_config.AA_TEXT,
                                                          (255, 255, 255))
                            FrameText -= 1
                        else:
                            FrameNotText = 25

        match select:
            case 0:
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 130, WINDOWS_SIZE[1] // 2 - 150))
            case 1:
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 130, WINDOWS_SIZE[1] // 2 - 100))
            case 2:
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 130, WINDOWS_SIZE[1] // 2 - 50))
            case 3:
                arrowRect = arrowImage.get_rect(center=(WINDOWS_SIZE[0] // 2 - 135, WINDOWS_SIZE[1] // 2))

        sc.fill((0, 0, 0))
        sc.blit(TextUp, TextUp.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 150)))
        sc.blit(TextDown, TextDown.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 100)))
        sc.blit(TextLeft, TextLeft.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2 - 50)))
        sc.blit(TextRight, TextRight.get_rect(center=(WINDOWS_SIZE[0] // 2, WINDOWS_SIZE[1] // 2)))
        sc.blit(arrowImage, arrowRect)
        sc.blit(scene_menu.EKeyImage, scene_menu.EKeyImageRect)
        sc.blit(scene_menu.TextOK, scene_menu.TextOKRect)
        sc.blit(QKeyImage, QKeyImageRect)
        sc.blit(TextQUIT, TextQUITRect)
        fill_group.draw(sc)
        pygame.display.update()
        clock.tick(FPS)
        fill_group.update()
