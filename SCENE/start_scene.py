import pygame
from config_script import clock, sc, LANGUAGE_SET, createImage, WINDOWS_SIZE
from SCENE import scene_config, scene_menu


def StartGameAnim():
    FPS: int = 0
    running: bool = True

    image = createImage("ASSETS/SPRITE/UI/PAUSE MENU/ICON/4.png")
    size: int = 75
    forceSize: int = 975
    image = pygame.transform.scale(image, (size, size))
    imageRect = image.get_rect(center=(WINDOWS_SIZE[0]//2, 850))

    isDrawImage = True

    angle: int = 0
    y: int = 850

    Frame: int = 0

    color = (0, 0, 0)

    while running:
        if Frame >= 35:
            if imageRect.y > WINDOWS_SIZE[1]//2-20:
                y -= 30
            else:
                if size <= 19960:
                    size += forceSize
                    if size >= 2000:
                        isDrawImage = False
                        color = (255, 255, 255)
                else:
                    running = False
                    scene_config.switch_scene(scene_menu.scene_menu)
        else:
            Frame += 1
        sc.fill(color)
        if isDrawImage:
            image = createImage("ASSETS/SPRITE/UI/PAUSE MENU/ICON/4.png")
            image = pygame.transform.scale(image, (size, size))
            angle += 15
            image = pygame.transform.rotate(image, angle)
            imageRect = image.get_rect(center=(WINDOWS_SIZE[0] // 2, y))
        sc.blit(image, imageRect)
        pygame.display.update()
        clock.tick(FPS)


def autorPresent():
    FPS: int = 0
    running: bool = True
