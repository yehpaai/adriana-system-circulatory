from SCENE import scene_config
from config_script import *
from font_config import NovelTextName, NovelText, FontKILL
from LOADING import loading


def scene0():
    running: bool = True
    FPS: int = 60

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 256))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(295)

    NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(65, 570))

    TextSet = NovelText.render("Вай вай вай! Що за дічь відбувається?. Чому усі наче як бухали з друзями!?",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(455, 605))

    KeyText = FontKILL.render("Натисність любу клавішу", scene_config.AA_TEXT, (255, 255, 255))

    match LANGUAGE_SET:
        case "EN":
            KeyText = FontKILL.render("Press Any Key", scene_config.AA_TEXT, (255, 255, 255))
    KeyTextRect = KeyText.get_rect(center=(1150, 690))
    KeyText.set_alpha(120)

    PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
    PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))

    select = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                select += 1

        sc.fill((0, 0, 0))
        match select:
            case 0:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Хм, цікаво. А що взагалі відбувається? Ну добре мене усі хотять знищити. Але я бачив як і до інших приставали",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Hmm, interesting. And what is going on? Well, they want to destroy me. But I saw how others were molested",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Самій цікаво.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Наче як нічого серйозного не було",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("As if nothing serious had happened",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 3:
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/ZERO/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (458, 668))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Я чула що є людина, котра заправляє своєю організацією, а вони в свою чергу з'їдають людей.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "I heard that there is a person who runs his organization, and they, in turn, eat people",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 4:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Щось якось простенько. Та і не так лячно",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Something simple. But not so scary",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 5:
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/ZERO/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (458, 668))
                NameText = NovelTextName.render("ZERO:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Так, згодна. Могли б вже не знаю, дітей маленьких катувати.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Yes, I agree. They could, I don't know, torture small children.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 6:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Ну так. Добре, пішли може знайдемо когось із знайомих. Цікаво, чи знають вони щось більше",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Yes. Okay, let's go, maybe we'll find someone we know. I wonder if they know anything more",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 7:
                running = False
                scene_config.switch_scene(loading.loading)
        sc.blit(FrameImage, FrameImageRect)
        sc.blit(NameText, NameRect)
        sc.blit(TextSet, TextSetRect)
        sc.blit(KeyText, KeyTextRect)
        pygame.display.update()
        clock.tick(FPS)


def scene1():
    running: bool = True
    FPS: int = 60

    FrameImage = pygame.image.load("ASSETS/SPRITE/UI/Novel/Frame.png").convert_alpha()
    FrameImage = pygame.transform.scale(FrameImage, (WINDOWS_SIZE[0], 256))
    FrameImageRect = FrameImage.get_rect(center=(WINDOWS_SIZE[0] // 2, 660))
    FrameImage.set_alpha(285)

    NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
    NameRect = NameText.get_rect(center=(70, 570))

    PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
    PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))

    TextSet = NovelText.render("ХЕЙ!",
                               scene_config.AA_TEXT, (255, 255, 255))
    TextSetRect = TextSet.get_rect(center=(60, 615))

    KeyText = FontKILL.render("Натисність любу клавішу", scene_config.AA_TEXT, (255, 255, 255))

    match LANGUAGE_SET:
        case "EN":
            KeyText = FontKILL.render("Press Any Key", scene_config.AA_TEXT, (255, 255, 255))
    KeyTextRect = KeyText.get_rect(center=(1150, 690))
    KeyText.set_alpha(120)

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                scene_config.switch_scene(None)
            if e.type == pygame.KEYDOWN:
                select += 1

        sc.fill((0, 0, 0))
        match select:
            case 0:
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("ХЕЙ!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("HEY!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 1:
                NameText = NovelTextName.render("Іоанн:", scene_config.AA_TEXT, (255, 255, 255))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Га?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Hm?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
            case 2:
                NameText = NovelTextName.render("Ioann:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("О, привіт!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Hello!",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 3:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Привіт. Як справи?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Hello. How are you?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 4:
                NameText = NovelTextName.render("Ioann:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Та наче не погано. А в тебе?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("But it's not bad. What about you?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 5:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Чудово. Ось тільки що труп старий сховав. Він вже мене не тішить, "
                                                   "так як раніше.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Ideally. simply hide an ancient corpse. He no longer pleases me as before.",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 6:
                NameText = NovelTextName.render("Іоанн:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("Ех, ти як завжди. Можливо досить вже цим всим займатисся?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("Eh, you are as usual. Maybe it's enough to deal with all this?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 7:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render("...",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 8:
                NameText = NovelTextName.render("Іоанн:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/3.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("А ти не чув, що усі шукають якихся дивних людей?",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Haven't you heard that everyone is looking for some strange people?",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 9:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render("З усіх дивних, я тут бачу тільки тебе.",
                                                   scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Of all the strange ones, I see only you here.",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 10:
                NameText = NovelTextName.render("Іоанн:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Іронічно...",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Ironically...",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 11:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/1.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Навідайся до Анни. Вона точно має більше інформації ніж я",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Visit Anna. She definitely has more information than I do",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 12:
                NameText = NovelTextName.render("Ioann:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/IOANN/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (388, 538))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Добре. Давай удачі.",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Alright. Good Luck",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(1055, 350)))
            case 14:
                NameText = NovelTextName.render("QWERTY:", scene_config.AA_TEXT, (255, 255, 255))
                PortraitCharacter = pygame.image.load("ASSETS/SPRITE/UI/VisualNovel/QWERTY/2.png").convert()
                PortraitCharacter = pygame.transform.smoothscale(PortraitCharacter, (328, 498))
                match LANGUAGE_SET:
                    case "UA":
                        TextSet = NovelText.render(
                            "Ну а я навідаюсь особисто до Данни",
                            scene_config.AA_TEXT, (255, 255, 255))
                    case "EN":
                        TextSet = NovelText.render(
                            "Well, I personally visit Danna",
                            scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(PortraitCharacter, PortraitCharacter.get_rect(center=(200, 315)))
            case 13:
                NameText = NovelTextName.render("", scene_config.AA_TEXT, (255, 255, 255))
                TextSet = NovelText.render(
                            "...",
                            scene_config.AA_TEXT, (255, 255, 255))
            case 15:
                running = False
                scene_config.switch_scene(loading.loading)

        sc.blit(FrameImage, FrameImageRect)
        sc.blit(NameText, NameRect)
        sc.blit(TextSet, TextSetRect)
        sc.blit(KeyText, KeyTextRect)
        pygame.display.update()
        clock.tick(FPS)
